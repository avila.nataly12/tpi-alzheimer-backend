'use strict';

const { Swpush } = require('../../model/swpush');
const webpush = require('web-push');

const isValidObjectId = require('../../helpers/validate-object-id');
const { WebPushError } = require('web-push');

module.exports = class SwpushService {

    
    async process(req, res) {

        const saveClavePush = await this.saveClave(req);

        return res.status(200).send({ mensaje: 'Clave Guardada', id: saveClavePush._id });
    }

  

    saveClave(req) {
         const SwpushToCreate = new Swpush(req.body);

        return SwpushToCreate.save();
    }


  
    
    async processVerifico(req, res) {
         const idUsuario = req.params.idUsuario;

        const swpush = await this.getPorUsuario2(idUsuario);
        console.log(swpush.length);
         if(swpush.length==0){
            console.log("entro al 0");
        return res.status(404).send({ mensaje: `Nota ${idUsuario} no encontrado` });

        }else{
            console.log("no es 0");

    const swpFormatted = swpush.map(swp => this.hideProperties(swp));
    console.log(swpFormatted);

    return res.status(200).send(swpFormatted);
    
}
    
  


 }
        async processSend(req, res) {

        /**
 * 
 * {"publicKey":"BECAXe4bSGX5TzyaYYYcTj10x1Sz7kxbSqNa_eBgy68iZVnQCixyhoUN7l5NlGiDFn1zrX0r-yT48tjNri54C2Q",
 * "privateKey":"KejSBzyPPkZ7Z6LTkje8yXrCAGX3M6hX6-ngK0NNC8o"}
 * 
 */

        const vapidKeys = {
            "publicKey": "BECAXe4bSGX5TzyaYYYcTj10x1Sz7kxbSqNa_eBgy68iZVnQCixyhoUN7l5NlGiDFn1zrX0r-yT48tjNri54C2Q",
            "privateKey": "KejSBzyPPkZ7Z6LTkje8yXrCAGX3M6hX6-ngK0NNC8o"
        }
        
        webpush.setVapidDetails(
            'mailto:mauayala@alumno.unlam.edu.ar',
            vapidKeys.publicKey,
            vapidKeys.privateKey
        );

        

        const idUsuario = req.body.idUsuario;
        const cuerpo = req.body.cuerpo;
        const titulo = req.body.titulo;
        const link = req.body.link;
    

        const swpush = await this.getPorUsuario(idUsuario);
        const encontrado = this.hideProperties(swpush);
 
 
        const playload ={
            "notification":{
                "title":titulo,
                "body":cuerpo,
                "vibrate":[100,50,100],
                "image": "https://alfahelp.com.ar/home.jpg",
                "actions":[{
                    "action":"explore",
                    "title":titulo
                }],
                "data":{"onActionClick":{
                    "default":{ "operation": 'openWindow' },
                    "explore":{ "operation": 'focusLastFocusedOrOpen', "url": link ,}
                }}
            }
        }
 

        const pushSubscription = {
            endpoint: 'https://fcm.googleapis.com/fcm/send/fanLZFxvOEQ:APA91bGpR2cHCCbqp3iabR13xd0rXHz3iH9dhhcQLcAneluJOYULNym6w8pCBR71goHIEwcCMwM-Ylrk-orKkYAPOtGSn8f92aiBGQkpp_mjNfe6K9nOd-kbO3Cx_y9gRDYXp8fZIOrp',
            keys: {
                auth: 'RcLncCvMlvbRpZRg3mFKiw',
                p256dh: 'BC8ykwylbA3mxep_UPe_i2VZmWgXuGVTgf97JqcZdu6uTi4w-HkiBwQ7X1bmrtbXYmHEFyGly7DFgkC1tiTtPTk'
            }
        }; 

     

        console.log(encontrado.clave);
        webpush.sendNotification(
            encontrado.clave,
            JSON.stringify(playload))
            .then(res => {
                console.log('Enviado !!');
            }).catch(err => {
                console.log('Error', err);
            })
    
        res.send({ data: 'Se envio subscribete!!' })
    
    }

    getPorUsuario(idUsuarioFORM) {
        return Swpush.findOne({idUsuario:idUsuarioFORM}).sort({'fechaCreacion': -1});
        
    }

    
    getPorUsuario2(idUsuarioFORM) {
        return Swpush.find({idUsuario:idUsuarioFORM}).sort({'fechaCreacion': -1});
        
    }


    hideProperties(swpush) {

        const { __v, _id, ...restOfSwpush } = swpush._doc;
        swpush.id = _id;
    
        return restOfSwpush;
    }


}