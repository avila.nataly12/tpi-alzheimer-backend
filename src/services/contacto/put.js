'use strict';

const { Contacto } = require('../../model/contacto');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class ContactoPutService {

    async process(req, res) {

        const idContacto = req.params.id;

        if(!idContacto)
            return res.status(400).send({ mensaje: 'El id del contacto es requerido' });

        if(!isValidObjectId(idContacto))
            return res.status(400).send({ mensaje: 'El id del contacto debe ser de tipo ObjectId' });

        const contacto = await this.getContacto(idContacto);

        if(!contacto)
            return res.status(404).send({ mensaje: `contacto ${idContacto} no encontrado` });

 
            console.log(req.body.paciente);
            console.log(req.body.cuidador);
            contacto.nombre = req.body.nombre;
            contacto.paciente = req.body.paciente;
            contacto.cuidador = req.body.cuidador;
            contacto.telefono = req.body.telefono;
            contacto.estado = req.body.estado;

        await contacto.save();

        return res.status(200).send({ id: idContacto });
    }

    getContacto(id) {
        return Contacto.findById(id);
    }


    
}