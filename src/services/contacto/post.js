'use strict';

const { Contacto } = require('../../model/contacto');
const { User } = require('../../model/user');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class ContactoPostService {

    async process(req, res) {

        const contactoCreated = await this.createContacto(req);

        return res.status(200).send({ mensaje: 'Contacto creada', id: contactoCreated._id });
    }

     
    createContacto(req) {

        const contactoToCreate = new Contacto(req.body);

        return contactoToCreate.save();
    }
}