'use strict';

const { Contacto } = require('../../model/contacto');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class ContactoGetService {

    async process(req, res) {

        const idContacto = req.params.idPaciente;

        if(!idContacto)
            return res.status(400).send({ mensaje: 'El id del contacto es requerido' });

        if(!isValidObjectId(idContacto))
            return res.status(400).send({ mensaje: 'El id del contacto debe ser de tipo ObjectId' });

        const contactos = await this.getContacto(idContacto);

        if(!contactos)
            return res.status(404).send({ mensaje: `contacto ${idContacto} no encontrado` });

       // const contactoFormatted = this.hideProperties(contacto);
        const contactoFormatted = contactos.map(contacto => this.hideProperties(contacto));

        return res.status(200).send(contactoFormatted);
    }

    getContacto(idContacto) {
        return Contacto.find({idPaciente:idContacto});
    }

    hideProperties(contacto) {

        const { __v, _id, ...restOfNota } = contacto._doc;
        restOfNota.id = _id;
    
        return restOfNota;
    }



    async processIdOrigen(req, res) {

        const idOrigenF = req.params.idOrigen;
        const idPacienteF = req.params.idPaciente;
        
        if(!idOrigenF)
            return res.status(400).send({ mensaje: 'El id del contacto es requerido' });

        if(!isValidObjectId(idOrigenF))
            return res.status(400).send({ mensaje: 'El id del contacto debe ser de tipo ObjectId' });

        const contactos = await this.getContactoIdOrigen(idOrigenF,idPacienteF);

        if(!contactos)
            return res.status(404).send({ mensaje: `contacto ${idOrigen} no encontrado` });

        const contactoFormatted = contactos.map(contacto => this.hideProperties(contacto));

        return res.status(200).send(contactoFormatted);
    }

    getContactoIdOrigen(idOrigenF,idPacienteF) {
        return Contacto.find({idOrigen:idOrigenF,idPaciente:idPacienteF,estado:true});
    }


    async dameContacto(req, res) {

        const idContacto = req.params.idContacto;
         
        if(!idContacto)
            return res.status(400).send({ mensaje: 'El id del contacto es requerido' });

      
        const contacto = await this.getPorIdContacto(idContacto);

        if(!contacto)
            return res.status(404).send({ mensaje: `contacto ${idContacto} no encontrado` });

        const contactoFormatted = this.hideProperties(contacto);

        return res.status(200).send(contactoFormatted);
    }

    getPorIdContacto(idContacto) {
        return Contacto.findById(idContacto);
    }
    

    async processBoton(req, res) {

         const idPacienteF = req.params.idPaciente;
        
        if(!idPacienteF)
            return res.status(400).send({ mensaje: 'El id del contacto es requerido' });

        if(!isValidObjectId(idPacienteF))
            return res.status(400).send({ mensaje: 'El id del contacto debe ser de tipo ObjectId' });

        const contactos = await this.getContactoBoton(idPacienteF);

        if(!contactos)
            return res.status(404).send({ mensaje: `contacto ${idOrigen} no encontrado` });

        const contactoFormatted = contactos.map(contacto => this.hideProperties(contacto));

        return res.status(200).send(contactoFormatted);
    }

    getContactoBoton(idPacienteF) {
        return Contacto.find({idPaciente:idPacienteF,estado:true,paciente:"true"});
    }

    


    
    async processlistadoOrigen(req, res) {

        const idOrigenF = req.params.idOrigen;
         
        if(!idOrigenF)
            return res.status(400).send({ mensaje: 'El id del contacto es requerido' });

        if(!isValidObjectId(idOrigenF))
            return res.status(400).send({ mensaje: 'El id del contacto debe ser de tipo ObjectId' });

        const contactos = await this.getContactoIdOrigenlISTADO(idOrigenF);

        if(!contactos)
            return res.status(404).send({ mensaje: `contacto ${idOrigen} no encontrado` });

        const contactoFormatted = contactos.map(contacto => this.hideProperties(contacto));

        return res.status(200).send(contactoFormatted);
    }

    getContactoIdOrigenlISTADO(idOrigenF) {
        return Contacto.find({idOrigen:idOrigenF,estado:true});
    }



    async processlistadoOrigenCuidador(req, res) {

        const idOrigenF = req.params.idOrigen;
         
        if(!idOrigenF)
            return res.status(400).send({ mensaje: 'El id del contacto es requerido' });

        if(!isValidObjectId(idOrigenF))
            return res.status(400).send({ mensaje: 'El id del contacto debe ser de tipo ObjectId' });

        const contactos = await this.getContactoIdOrigenlISTADOCuidador(idOrigenF);

        if(!contactos)
            return res.status(404).send({ mensaje: `contacto ${idOrigen} no encontrado` });

        const contactoFormatted = contactos.map(contacto => this.hideProperties(contacto));

        return res.status(200).send(contactoFormatted);
    }

    getContactoIdOrigenlISTADOCuidador(idOrigenF) {
        return Contacto.find({idOrigen:idOrigenF,estado:true,cuidador:'true'});
    }



    

    async processlistadoOrigenPaciente(req, res) {

        const idOrigenF = req.params.idOrigen;
         
        if(!idOrigenF)
            return res.status(400).send({ mensaje: 'El id del contacto es requerido' });

        if(!isValidObjectId(idOrigenF))
            return res.status(400).send({ mensaje: 'El id del contacto debe ser de tipo ObjectId' });

        const contactos = await this.getContactoIdOrigenlISTADOPaciente(idOrigenF);

        if(!contactos)
            return res.status(404).send({ mensaje: `contacto ${idOrigen} no encontrado` });

        const contactoFormatted = contactos.map(contacto => this.hideProperties(contacto));

        return res.status(200).send(contactoFormatted);
    }

    getContactoIdOrigenlISTADOPaciente(idOrigenF) {
        return Contacto.find({idOridPacienteigen:idOrigenF,estado:true,cuidador:'true'});
    }



}