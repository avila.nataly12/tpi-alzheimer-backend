'use strict';

const { Quesoy } = require('../../model/quesoy');
 
const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class QueSoyListService {
 

    async process(req, res) {

        const quesoy = await this.getQuesoy();
        const juegoFormatted = quesoy.map(juego => this.hideProperties(juego));

        return res.status(200).send(juegoFormatted);
    }


    
    async processExcepto(req, res) {
        const palabra = req.params.palabra;
        const quesoy = await this.getQuesoyExcepto(palabra);
        const juegoFormatted = quesoy.map(juego => this.hideProperties(juego));

        return res.status(200).send(juegoFormatted);
    }
 

  
    getQuesoyExcepto(palabra) {
        return Quesoy.find({nombre: palabra});
 
    }
    
    getQuesoy() {
        return Quesoy.find();
    }


    async processListMemoria(req, res) {
        const nivel = req.params.nivel;

        const juegoList = await this.getJuegosMemoria(nivel);
        const juegoFormatted = juegoList.map(juego => this.hideProperties(juego));
        return res.status(200).send(juegoFormatted);
    }
    
    
    getJuegosMemoria(nivel) {
        console.log(nivel);
        var cantidad = 7;
        if(nivel=="Fácil"){
             cantidad =5;
        }
        if(nivel=="Medio"){
             cantidad =6;
        }
        
        return Quesoy.find().limit(cantidad);
    }




    hideProperties(juego) {
        const { __v, _id, ...restOfJuego } = juego._doc;
        restOfJuego.id = _id;
    
        return restOfJuego;
    }
}