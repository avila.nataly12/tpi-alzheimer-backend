'use strict';

const { Album } = require('../../model/album');
const { User } = require('../../model/user');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class AlbumListByPacienteService {

    async process(req, res) {

        const userId = req.params.idPaciente;

        if(!userId)
            return res.status(400).send({ mensaje: 'El id del paciente es requerido' });

        if(!isValidObjectId(userId))
            return res.status(400).send({ mensaje: 'El id del paciente debe ser de tipo ObjectId' });

        const user = await this.getUser(userId);

        if(!user)
            return res.status(400).send({ mensaje: `No existe un usuario con el id ${userId}` });

        const albunes = await this.getAlbunes(userId);
        const albunesFormatted = albunes.map(album => this.hideProperties(album));

        return res.status(200).send(albunesFormatted);
    }

    getUser(userId) {
        return User.findById(userId);
    }

    getAlbunes(userId) {
        return Album.find({ idUsuarioDestino: userId,estado:true });
    }

    hideProperties(album) {

        const { __v, _id, ...restOfAlbum } = album._doc;
        restOfAlbum.id = _id;
    
        return restOfAlbum;
    }
}