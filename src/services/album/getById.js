'use strict';

const { Album } = require('../../model/album');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class AlbumGetByIdService {

    async process(req, res) {

        const idAlbum = req.params.id;

        if(!idAlbum)
            return res.status(400).send({ mensaje: 'El id del album es requerido' });

        if(!isValidObjectId(idAlbum))
            return res.status(400).send({ mensaje: 'El id del album debe ser de tipo ObjectId' });

        const album = await this.getAlbum(idAlbum);

        if(!album)
            return res.status(404).send({ mensaje: `Album ${idAlbum} no encontrado` });

        const albumFormatted = this.hideProperties(album);

        return res.status(200).send(albumFormatted);
    }

    getAlbum(idAlbum) {
        return Album.findById(idAlbum);
    }

    hideProperties(album) {

        const { __v, _id, ...restOfAlbum } = album._doc;
        restOfAlbum.id = _id;
    
        return restOfAlbum;
    }
}