'use strict';

const { Album } = require('../../model/album');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class AlbumPutService {

    async process(req, res) {

        const idAlbum = req.params.id;

        if(!idAlbum)
            return res.status(400).send({ mensaje: 'El id del album es requerido' });

        if(!isValidObjectId(idAlbum))
            return res.status(400).send({ mensaje: 'El id del album debe ser de tipo ObjectId' });

        const album = await this.getAlbum(idAlbum);

        if(!album)
            return res.status(404).send({ mensaje: `Album ${idAlbum} no encontrado` });

        const data = req.body;

        if(data.nombre)
            album.nombre = data.nombre;

        if(data.estado)
            album.estado = data.estado;

        await album.save();

        return res.status(200).send({ id: album._id });
    }

    getAlbum(idAlbum) {
        return Album.findById(idAlbum);
    }
}