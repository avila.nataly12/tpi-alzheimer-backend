'use strict';

const { Album } = require('../../model/album');

module.exports = class AlbumPostService {

    async process(req, res) {

        const data = req.body;

        if(!data.nombre)
            return res.status(400).send({ mensaje: 'El nombre del album es requerido '});

        if(!data.idUsuarioOrigen || !data.idUsuarioDestino)
            return res.status(400).send({ mensaje: 'El usuario de origen y usuario destino son requeridos'});

        const album = await this.createAlbum(data);

        return res.status(200).send({ mensaje: 'Album creado', id: album._id });
    }

    createAlbum(data) {

        const albumCreated = new Album(data);

        return albumCreated.save();
    }
}