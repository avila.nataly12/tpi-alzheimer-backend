'use strict';
 
const { Archivo } = require('../../model/archivo');
const { User } = require('../../model/user');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class ArchivoPostService {

    async process(req, res) {

        const archivoCreated = await this.createArchivo(req);

        return res.status(200).send({ mensaje: 'Archivo Creado', id: archivoCreated._id });
    }


    createArchivo(req) {
        const archivoToCreate = new Archivo(req.body);

        return archivoToCreate.save();
    }
 



    async processCambiarEstado(req, res) {
        const idArchivo = req.params.id;
        const archivoToEditar=await this.getArchivo(idArchivo);

        console.log(idArchivo._id);
        if(archivoToEditar.estado==true){
            archivoToEditar.estado=false;
             }else{
            archivoToEditar.estado=true;
    
          }
 
        await archivoToEditar.save();

        return res.status(200).send({ mensaje: 'Archivo editado', id: archivoToEditar._id });
    }
    
 

    
    getArchivo(id) {
         return Archivo.findById(id);
    }
}