'use strict';

const { Archivo } = require('../../model/archivo');
const { User } = require('../../model/user');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class ArchivoDeleteService {

    async process(req, res) {

        const id = req.params.id;
        const archivodelete = await this.getArchivo(id);

        await archivodelete.delete();
        
        }

  
    getArchivo(id) {
        return Archivo.findById(id);
    }
}