'use strict';

const { Archivo } = require('../../model/archivo');
const { User } = require('../../model/user');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class ArchivoListService {

     


    async processActivasUsuario(req, res) {
        const idUsuarioBuscar = req.params.idUsuario;
        const archivos = await this.getArchivosActivasUsuario(idUsuarioBuscar);
        const archivosFormatted = archivos.map(archivo => this.hideProperties(archivo));

        return res.status(200).send(archivosFormatted);
    }

    getArchivosActivasUsuario(idUsuario) {
      //  const mysort = { prioridad: 1 };
        const estado = true;
         return Archivo.find({ estado: estado , idUsuario: idUsuario});

    }
    
   


    
    async processActivasUsuarioAlbum(req, res) {
        const idUsuarioBuscar = req.params.idUsuario;
        const idAlbumBuscar = req.params.idAlbum;
 

        const archivos = await this.getArchivosActivasUsuarioAlbum(idUsuarioBuscar,idAlbumBuscar);
        const archivosFormatted = archivos.map(archivo => this.hideProperties(archivo));

        return res.status(200).send(archivosFormatted);
    }


    async buscounoarchivo(req, res) {
        const idUsuarioBuscar = req.params.idUsuario;
        const idAlbumBuscar = req.params.idAlbum;
 

        const archivos = await this.getArchivosActivasUsuarioAlbumUno(idUsuarioBuscar,idAlbumBuscar);

        const archivosFormatted = archivos.map(archivo => this.hideProperties(archivo));
 

        return res.status(200).send(archivosFormatted);
    }


    
    async processverFotoPerfil(req, res) {
        const nombre = req.params.nombre;
 

        const archivo = await this.getOneArchivosActivasUsuarioAlbum(nombre);
        if(!archivo){
            return res.status(404).send({ mensaje: `archivo ${nombre} no encontrado` });

        }
        const archivosFormatted = this.hideProperties(archivo);


        return res.status(200).send(archivosFormatted);
    }
    getArchivosActivasUsuarioAlbum(idUsuario,idAlbumBuscar) {
         
         return Archivo.find({idUsuario: idUsuario,idAlbum:idAlbumBuscar});

    }


    getArchivosActivasUsuarioAlbumUno(idUsuario,idAlbumBuscar) {
         
        return Archivo.find({idUsuario: idUsuario,idAlbum:idAlbumBuscar}).limit(1);

   }


    getOneArchivosActivasUsuarioAlbum(nombre) {
         
        return Archivo.findOne({nombre: nombre}).sort({'fechaCreacion': -1});

   }


    
    async processverMultimedia(req, res) {
        const idUsuarioOrigen = req.params.idUsuarioOrigen;
 
        const archivos = await this.getArchivosVerMultimedia(idUsuarioOrigen);
        const archivosFormatted = archivos.map(archivo => this.hideProperties(archivo));

        return res.status(200).send(archivosFormatted);
    }

    getArchivosVerMultimedia(idUsuarioOrigen) {
         
         return Archivo.find({estado: true,idUsuarioOrigen:idUsuarioOrigen});

    }

   
 
    hideProperties(archivo) {

        const { __v, _id, ...restOfArchivo } = archivo._doc;
        restOfArchivo.id = _id;
    
        return restOfArchivo;
    }
}