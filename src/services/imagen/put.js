'use strict';

const { Imagen } = require('../../model/imagen');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class ImagenPutService {

    async process(req, res) {

        const idImagen = req.params.id;

        if(!idImagen)
            return res.status(400).send({ mensaje: 'El id de la imagen es requerido' });

        if(!isValidObjectId(idImagen))
            return res.status(400).send({ mensaje: 'El id de la imagen debe ser de tipo ObjectId' });

        const imagen = await this.getImagen(idImagen);

        if(!imagen)
            return res.status(404).send({ mensaje: `Imagen ${idImagen} no encontrada` });

        const data = req.body;

        if(data.url)
            imagen.url = data.url;

        if(data.descripcion)
            imagen.descripcion = data.descripcion;

        if(data.idAlbum)
            imagen.idAlbum = data.idAlbum;

        if(data.estado)
            imagen.estado = data.estado;

        await imagen.save();

        return res.status(200).send({ id: imagen._id });
    }

    getImagen(idImagen) {
        return Imagen.findById(idImagen);
    }
}