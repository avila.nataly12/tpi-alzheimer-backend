'use strict';

const { Imagen } = require('../../model/imagen');

module.exports = class ImagenPostService {

    async process(req, res) {

        const data = req.body;

        if(!data.url)
            return res.status(400).send({ mensaje: 'La url de la imagen es requerida' });

        if(!data.idAlbum)
            return res.status(400).send({ mensaje: 'El id del album al que pertenece la imagen es requerido' });

        if(!data.descripcion)
            return res.status(400).send({ mensaje: 'La descripcion de la imagen es requerida' });

        const imagen = await this.createImagen(data);

        return res.status(200).send({ mensaje: 'Imagen creada', id: imagen._id });
    }

    createImagen(data) {

        const imagenCreated = new Imagen(data);

        return imagenCreated.save();
    }
}