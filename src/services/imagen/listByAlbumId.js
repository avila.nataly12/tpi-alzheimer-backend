'use strict';

const { Imagen } = require('../../model/imagen');
const { Album } = require('../../model/album');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class ImagenListByAlbumIdService {

    async process(req, res) {

        const idAlbum = req.params.idAlbum;

        if(!idAlbum)
            return res.status(400).send({ mensaje: 'El id del album es requerido' });

        if(!isValidObjectId(idAlbum))
            return res.status(400).send({ mensaje: 'El id del album debe ser de tipo ObjectId' });

        const album = await this.getAlbum(idAlbum);

        if(!album)
            return res.status(400).send({ mensaje: `No existe un album con el id ${idAlbum}` });

        const imagenes = await this.getImagenes(idAlbum);
        const imagenesFormatted = imagenes.map(imagen => this.hideProperties(imagen));

        return res.status(200).send(imagenesFormatted);
    }

    getAlbum(idAlbum) {
        return Album.findById(idAlbum);
    }

    getImagenes(idAlbum) {
        return Imagen.find({ idAlbum });
    }

    hideProperties(imagen) {

        const { __v, _id, ...restOfImagen } = imagen._doc;
        restOfImagen.id = _id;
    
        return restOfImagen;
    }
}