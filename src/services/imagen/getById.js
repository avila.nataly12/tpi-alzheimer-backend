'use strict';

const { Imagen } = require('../../model/imagen');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class ImagenGetByIdService {

    async process(req, res) {

        const idImagen = req.params.id;

        if(!idImagen)
            return res.status(400).send({ mensaje: 'El id de la imagen es requerido' });

        if(!isValidObjectId(idImagen))
            return res.status(400).send({ mensaje: 'El id de la imagen debe ser de tipo ObjectId' });

        const imagen = await this.getImagen(idImagen);

        if(!imagen)
            return res.status(404).send({ mensaje: `Imagen ${idImagen} no encontrada` });

        const imagenFormatted = this.hideProperties(imagen);

        return res.status(200).send(imagenFormatted);
    }

    getImagen(idImagen) {
        return Imagen.findById(idImagen);
    }

    hideProperties(imagen) {

        const { __v, _id, ...restOfImagen } = imagen._doc;
        restOfImagen.id = _id;
    
        return restOfImagen;
    }
}