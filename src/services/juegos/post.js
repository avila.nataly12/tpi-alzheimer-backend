'use strict';

const { JuegoHabilitados } = require('../../model/juegosHabilitados');
const { Reporte } = require('../../model/reporte');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class JuegoPostService {

    async process(req, res) {

   
        const juegosEstadosActualizar = await this.actualizarEstadosJuegos(req);

        return res.status(200).send({ mensaje: 'Correcto', id: juegosEstadosActualizar._id });
    }

      
    async processReporte(req, res) {

        const reporteResultado = await this.guardarResultado(req);

        return res.status(200).send({ mensaje: 'Correcto', id: reporteResultado._id });
    }

    async guardarResultado(req) {
        const reporteToCreate = new Reporte(req.body);

        return reporteToCreate.save();
    }

    async actualizarEstadosJuegos(req) {
        console.log("1");
        var data = req.body;

      /*  if(data.nivel=="no"){
          //eliminar
       const juegoHabilitado = await JuegoHabilitados.findById(data.idHabilitado);
  //     return juegoHabilitado.delete();

        }else{

            */
      if(data.idHabilitado=="null"){
                    if(data.idJuego!="null"){
                const juegoHabilitadosToCreate = new JuegoHabilitados(req.body);
                return juegoHabilitadosToCreate.save();
                console.log("2");

                     }
            }  else{
 
        const juegoHabilitado = await this.getJuegoHabilitado(data.idHabilitado);
        juegoHabilitado.set({
            nivel:data.nivel
        }); 

         return juegoHabilitado.save();
               } 
      //  }

    }


    getJuegoHabilitado(idHabilitado) {
        return JuegoHabilitados.findById(idHabilitado);
    }
}