'use strict';

const { Juego } = require('../../model/juegos');
const { Reporte } = require('../../model/reporte');
const { JuegoHabilitados } = require('../../model/juegosHabilitados');
 
const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class JuegosListService {
 

    async process(req, res) {

        const juegoList = await this.getJuegos();
        const juegoFormatted = juegoList.map(juego => this.hideProperties(juego));
        return res.status(200).send(juegoFormatted);
    }

    
    async reportes(req, res) {

        const reporteList = await this.getReportes();
        const reporteFormatted = reporteList.map(reporte => this.hideProperties(reporte));
        return res.status(200).send(reporteFormatted);
    }
    async reportesPorUsuario(req, res) {
        const idJuego = req.params.idJuego;
        const idPaciente = req.params.idPaciente;
        const reporteList = await this.getReportesPorUsuario(idJuego,idPaciente);
        const reporteFormatted = reporteList.map(reporte => this.hideProperties(reporte));
        return res.status(200).send(reporteFormatted);
    }
    


    async reportesPorUsuarioFecha(req, res) {
        const idJuego = req.params.idJuego;
        const idPaciente = req.params.idPaciente;
        const desde = req.params.desde;
        const hasta = req.params.hasta;
        const reporteList = await this.getReportesPorUsuarioFecha(idJuego,idPaciente,desde,hasta);
        const reporteFormatted = reporteList.map(reporte => this.hideProperties(reporte));
        return res.status(200).send(reporteFormatted);
    }
    


    

    async processHabilitados(req, res) {
        const idUsuarioBuscar = req.params.idUsuario;
        const juegoHabilitado = await this.getJuegosHabilitados(idUsuarioBuscar);
        const juegoFormatted = juegoHabilitado.map(juego => this.hideProperties(juego));

        return res.status(200).send(juegoFormatted);
    }


        
    async processDameJuego(req, res) {
        const idJuegoBuscar = req.params.idJuego;
        const juegoHabilitado = await this.getDamejuego(idJuegoBuscar);
         const juegoInfo = this.hideProperties(juegoHabilitado);

        return res.status(200).send(juegoInfo);
    }
    
    
     
    getDamejuego(idJuegoBuscar) { 
        return Juego.findById(idJuegoBuscar);
    }
     
    getJuegos() {
        return Juego.find();
    }

        
    getReportes() {
        return Reporte.find();
    }


    getReportesPorUsuario(idJuegof,idPacientef) {
        return Reporte.find({idJuego:idJuegof,idPaciente:idPacientef});
    }


    getReportesPorUsuarioFecha(idJuegof,idPacientef,desde,hasta) {
        console.log(desde);

        return Reporte.find({idJuego:idJuegof,idPaciente:idPacientef,
            fechaCreacion: {$gte: new Date(desde).toISOString(),$lt: new Date(hasta).toISOString() }
         });
    }



    getJuegosHabilitados(idUsuarioBuscar) {
        return JuegoHabilitados.find({idUsuario:idUsuarioBuscar});
    }


    hideProperties(juego) {
        const { __v, _id, ...restOfJuego } = juego._doc;
        restOfJuego.id = _id;
    
        return restOfJuego;
    }

    
}