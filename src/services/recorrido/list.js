'use strict';

const { Recorrido } = require('../../model/recorrido');
const { User } = require('../../model/user');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class RecorridoListService {

    async process(req, res) {

        const userId = req.params.userId;

        if(!userId)
            return res.status(400).send({ mensaje: 'El id del usuario es requerido' });

        if(!isValidObjectId(userId))
            return res.status(400).send({ mensaje: 'El id del usuario debe ser de tipo ObjectId' });

        const user = await this.getUser(userId);

        if(!user)
            return res.status(400).send({ mensaje: `No existe un usuario con el id ${userId}` });

        const recorridos = await this.getRecorridos(userId);
        const recorridosFormatted = recorridos.map(recorrido => this.hideProperties(recorrido));

        return res.status(200).send(recorridosFormatted);
    }

    getUser(userId) {
        return User.findById(userId);
    }

    getRecorridos(userId) {
        return Recorrido.find({ idUsuarioDestino: userId });
    }

    hideProperties(recorrido) {

        const { __v, _id, ...restOfRecorrido } = recorrido._doc;
        restOfRecorrido.id = _id;
    
        return restOfRecorrido;
    }
}