'use strict';

const { Recorrido } = require('../../model/recorrido');
const { User } = require('../../model/user');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class RecorridoPostService {

    async process(req, res) {

        const { isBadRequest, errorMesagge } = await this.validateRequest(req);

        if(isBadRequest)
            return res.status(400).send({ mensaje: errorMesagge });

        const recorridoCreated = await this.createRecorrido(req);

        return res.status(200).send({ mensaje: 'Recorrido creado', id: recorridoCreated._id });
    }

    async validateRequest(req) {

        let isBadRequest = false;
        let errorMesagge = '';

        const {
            idUsuarioOrigen, idUsuarioDestino, estado, coordenadas,
            fechaCreacion, fechaEstimadaPartida, fechaEstimadaLlegada, horaEstimadaPartida, horaEstimadaLlegada
        } = req.body;

        if(!idUsuarioOrigen || !idUsuarioDestino) {

            isBadRequest = true;
            errorMesagge = 'El id del usuario de origen y el usuario de destino son requeridos'

            return { isBadRequest, errorMesagge };
        }

        if(!isValidObjectId(idUsuarioOrigen) || !isValidObjectId(idUsuarioDestino)) {

            isBadRequest = true;
            errorMesagge = 'El id del usuario de origen y el usuario de destino deben ser de tipo ObjectId'

            return { isBadRequest, errorMesagge };
        }

        const areValidUsersIds = await this.validateUserIds(idUsuarioOrigen, idUsuarioDestino);

        if(!areValidUsersIds) {
            isBadRequest = true;
            errorMesagge = 'Ids de usuarios (origen - destino) invalidos';

            return { isBadRequest, errorMesagge };
        }

        if(!coordenadas || !Array.isArray(coordenadas)) {

            isBadRequest = true;
            errorMesagge = 'La coordenadas del recorrido son requeridas'

            return { isBadRequest, errorMesagge };
        }

        if(!fechaEstimadaPartida || !fechaEstimadaLlegada || !horaEstimadaPartida || !horaEstimadaLlegada) {

            isBadRequest = true;
            errorMesagge = 'Las fechas y horas estimadas de partida y llegada son obligatorias'

            return { isBadRequest, errorMesagge };
        }

        return { isBadRequest, errorMesagge };
    }

    async validateUserIds(idUsuarioOrigen, idUsuarioDestino) {

        const userIds = [...new Set([idUsuarioOrigen, idUsuarioDestino])];
        const userQuantity = userIds.length;

        if(userQuantity === 1) {
            const userFound = await User.findById(idUsuarioOrigen);

            if(userFound)
                return true;
        }

        if(userQuantity === 2) {
            const [userOrigenFound, userDestinoFound] = await Promise.all([
                User.findById(idUsuarioOrigen),
                User.findById(idUsuarioDestino),
            ]);

            if(userOrigenFound && userDestinoFound)
                return true;
        }

        return false;
    }

    createRecorrido(req) {

        const recorridoToCreate = new Recorrido(req.body);

        return recorridoToCreate.save();
    }
}