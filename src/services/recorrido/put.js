'use strict';

const { Recorrido } = require('../../model/recorrido');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class RecorridoPutService {

    async process(req, res) {

        const idRecorrido = req.params.id;

        if(!idRecorrido)
            return res.status(400).send({ mensaje: 'El id del recorrido es requerido' });

        if(!isValidObjectId(idRecorrido))
            return res.status(400).send({ mensaje: 'El id del recorrido debe ser de tipo ObjectId' });

        const recorrido = await this.getRecorrido(idRecorrido);

        if(!recorrido)
            return res.status(404).send({ mensaje: `No se encontro el recorrido ${idRecorrido}` });

        const nuevoEstado = req.body.estado;

        if(!nuevoEstado)
            return res.status(400).send({ mensaje: "Indicar el estado del recorrido es requerido" });

        recorrido.estado = nuevoEstado;

        await recorrido.save();

        return res.status(200).send({ id: recorrido._id });
    }

    getRecorrido(idRecorrido) {
        return Recorrido.findById(idRecorrido);
    }
}