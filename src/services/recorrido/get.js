'use strict';

const { Recorrido } = require('../../model/recorrido');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class RecorridoGetService {

    async process(req, res) {

        const idRecorrido = req.params.id;

        if(!idRecorrido)
            return res.status(400).send({ mensaje: 'El id del recorrido es requerido' });

        if(!isValidObjectId(idRecorrido))
            return res.status(400).send({ mensaje: 'El id del recorrido debe ser de tipo ObjectId' });

        const recorrido = await this.getRecorrido(idRecorrido);

        if(!recorrido)
            return res.status(404).send({ mensaje: `Recorrido ${idRecorrido} no encontrado` });

        const recorridoFormatted = this.hideProperties(recorrido);

        return res.status(200).send(recorridoFormatted);
    }

    getRecorrido(idRecorrido) {
        return Recorrido.findById(idRecorrido);
    }

    hideProperties(recorrido) {

        const { __v, _id, ...restOfRecorrido } = recorrido._doc;
        restOfRecorrido.id = _id;
    
        return restOfRecorrido;
    }
}