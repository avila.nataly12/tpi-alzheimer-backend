'use strict';

const { Recorrido } = require('../../model/recorrido');

module.exports = class RecorridoListService {

    async process(req, res) {

        const recorridos = await this.getRecorridos();
        const recorridosFormatted = recorridos.map(recorrido => this.hideProperties(recorrido));

        return res.status(200).send(recorridosFormatted);
    }

    getRecorridos() {
        return Recorrido.find();
    }

    hideProperties(recorrido) {

        const { __v, _id, ...restOfRecorrido } = recorrido._doc;
        restOfRecorrido.id = _id;
    
        return restOfRecorrido;
    }
}