'use strict';

const { DatosSalud } = require('../../model/datosSalud');
const { User } = require('../../model/user');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class DatosSaludListByPacienteService {

    async process(req, res) {

        const userId = req.params.idPaciente;

        if(!userId)
            return res.status(400).send({ mensaje: 'El id del paciente es requerido' });

        if(!isValidObjectId(userId))
            return res.status(400).send({ mensaje: 'El id del paciente debe ser de tipo ObjectId' });

        const user = await this.getUser(userId);

        if(!user)
            return res.status(400).send({ mensaje: `No existe un usuario con el id ${userId}` });

        const datosSalud = await this.getDatosSalud(userId);
        const datosSaludFormatted = datosSalud.map(datoSalud => this.hideProperties(datoSalud));

        return res.status(200).send(datosSaludFormatted);
    }

    getUser(userId) {
        return User.findById(userId);
    }

    getDatosSalud(userId) {
        return DatosSalud.find({ idPaciente: userId }).sort({'fecha': -1});
    }

    hideProperties(datoSalud) {

        const { __v, _id, ...restOfDatoSalud } = datoSalud._doc;
        restOfDatoSalud.id = _id;
    
        return restOfDatoSalud;
    }


    async processFechas(req, res) {

        const userId = req.params.idPaciente;
        const desde = req.params.desde;
        const hasta = req.params.hasta;

        if(!userId)
            return res.status(400).send({ mensaje: 'El id del paciente es requerido' });

        if(!isValidObjectId(userId))
            return res.status(400).send({ mensaje: 'El id del paciente debe ser de tipo ObjectId' });

        const user = await this.getUser(userId);

        if(!user)
            return res.status(400).send({ mensaje: `No existe un usuario con el id ${userId}` });

        const datosSalud = await this.getDatosSaludFechas(userId,desde,hasta);
        const datosSaludFormatted = datosSalud.map(datoSalud => this.hideProperties(datoSalud));

        return res.status(200).send(datosSaludFormatted);
    }

    getDatosSaludFechas(userId,desde,hasta) {
        return DatosSalud.find({ idPaciente: userId ,
            fecha: {$gte: new Date(desde).toISOString(),$lt: new Date(hasta).toISOString() }
        }).sort({'fecha': -1});
    }


}