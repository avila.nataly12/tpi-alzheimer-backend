'use strict';

const { DatosSalud } = require('../../model/datosSalud');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class DatosSaludPutService {

    async process(req, res) {

        const idDatosSalud = req.params.id;

        if(!idDatosSalud)
            return res.status(400).send({ mensaje: 'El id de los datos de salud es requerido' });

        if(!isValidObjectId(idDatosSalud))
            return res.status(400).send({ mensaje: 'El id de los datos de salud debe ser de tipo ObjectId' });

        const datosDeSalud = await this.getDatosDeSalud(idDatosSalud);

        if(!datosDeSalud)
            return res.status(404).send({ mensaje: `Datos de salud ${idDatosSalud} no encontrado` });

        datosDeSalud.presionsistolica = req.body.presionsistolica;
        datosDeSalud.presiondiastolica = req.body.presiondiastolica;
        datosDeSalud.oxigeno = req.body.oxigeno;
        datosDeSalud.temperatura = req.body.temperatura;
        datosDeSalud.fecha = req.body.fecha;
        datosDeSalud.hora = req.body.hora;

        await datosDeSalud.save();

        return res.status(200).send({ id: datosDeSalud._id });
    }

    getDatosDeSalud(idDatosSalud) {
        return DatosSalud.findById(idDatosSalud);
    }
}