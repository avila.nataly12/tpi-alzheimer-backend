'use strict';

const { DatosSalud } = require('../../model/datosSalud');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class DatosSaludPostService {

    async process(req, res) {

        const datosSalud = await this.createDatosSalud(req);

        return res.status(200).send({ mensaje: 'Datos de salud creado', id: datosSalud._id });
    }

    createDatosSalud(req) {

        const datosSaludCreated = new DatosSalud(req.body);

        return datosSaludCreated.save();
    }
}