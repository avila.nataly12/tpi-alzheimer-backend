'use strict';

const { DatosSalud } = require('../../model/datosSalud');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class DatosSaludGetService {

    async process(req, res) {

        const idDatosSalud = req.params.id;

        if(!idDatosSalud)
            return res.status(400).send({ mensaje: 'El id de los datos de salud es requerido' });

        if(!isValidObjectId(idDatosSalud))
            return res.status(400).send({ mensaje: 'El id de los datos de salud debe ser de tipo ObjectId' });

        const datosSalud = await this.getDatosSalud(idDatosSalud);

        if(!datosSalud)
            return res.status(404).send({ mensaje: `Nota ${idDatosSalud} no encontrado` });

        const datosSaludFormatted = this.hideProperties(datosSalud);

        return res.status(200).send(datosSaludFormatted);
    }

    getDatosSalud(idDatosSalud) {
        return DatosSalud.findById(idDatosSalud);
    }

    hideProperties(datosSalud) {

        const { __v, _id, ...restOfDatosSalud } = datosSalud._doc;
        restOfDatosSalud.id = _id;
    
        return restOfDatosSalud;
    }
}