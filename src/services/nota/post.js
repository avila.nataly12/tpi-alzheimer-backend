'use strict';

const { Nota } = require('../../model/nota');
const { User } = require('../../model/user');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class NotaPostService {

    async process(req, res) {

        const { isBadRequest, errorMesagge } = await this.validateRequest(req);

        if(isBadRequest)
            return res.status(400).send({ mensaje: errorMesagge });

        const notaCreated = await this.createNota(req);

        return res.status(200).send({ mensaje: 'Nota creada', id: notaCreated._id });
    }

    async validateRequest(req) {

        let isBadRequest = false;
        let errorMesagge = '';

        const {
            idUsuarioOrigen, idUsuarioDestino, contenido, titulo
        } = req.body;

        if(!idUsuarioOrigen || !idUsuarioDestino) {

            isBadRequest = true;
            errorMesagge = 'El id del usuario de origen y el usuario de destino son requeridos';

            return { isBadRequest, errorMesagge };
        }

        if(!isValidObjectId(idUsuarioOrigen) || !isValidObjectId(idUsuarioDestino)) {

            isBadRequest = true;
            errorMesagge = 'El id del usuario de origen y el usuario de destino deben ser de tipo ObjectId';

            return { isBadRequest, errorMesagge };
        }

        const areValidUsersIds = await this.validateUserIds(idUsuarioOrigen, idUsuarioDestino);

        if(!areValidUsersIds) {
            isBadRequest = true;
            errorMesagge = 'Ids de usuarios (origen - destino) invalidos';

            return { isBadRequest, errorMesagge };
        }

        if(!contenido) {

            isBadRequest = true;
            errorMesagge = 'El contenido de la nota es requerido';

            return { isBadRequest, errorMesagge };
        }

        if(!titulo) {

            isBadRequest = true;
            errorMesagge = 'El titulo de la nota es requerido';

            return { isBadRequest, errorMesagge };
        }

        return { isBadRequest, errorMesagge };
    }

    async validateUserIds(idUsuarioOrigen, idUsuarioDestino) {

        const userIds = [...new Set([idUsuarioOrigen, idUsuarioDestino])];
        const userQuantity = userIds.length;

        if(userQuantity === 1) {
            const userFound = await User.findById(idUsuarioOrigen);

            if(userFound)
                return true;
        }

        if(userQuantity === 2) {
            const [userOrigenFound, userDestinoFound] = await Promise.all([
                User.findById(idUsuarioOrigen),
                User.findById(idUsuarioDestino),
            ]);

            if(userOrigenFound && userDestinoFound)
                return true;
        }

        return false;
    }

    createNota(req) {

        const notaToCreate = new Nota(req.body);

        return notaToCreate.save();
    }
}