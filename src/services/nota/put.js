'use strict';

const { Nota } = require('../../model/nota');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class NotaPutService {

    async process(req, res) {

        const idNota = req.params.id;

        if(!idNota)
            return res.status(400).send({ mensaje: 'El id del nota es requerido' });

        if(!isValidObjectId(idNota))
            return res.status(400).send({ mensaje: 'El id del nota debe ser de tipo ObjectId' });

        const nota = await this.getNota(idNota);

        if(!nota)
            return res.status(404).send({ mensaje: `Nota ${idNota} no encontrado` });

        const nuevoEstadoNota = req.body.estado;

        if(typeof nuevoEstadoNota !== "boolean")
            return res.status(400).send({ mensaje: "El estado de la nota es requerido y debe ser booleano" });

        nota.estado = nuevoEstadoNota;

        await nota.save();

        return res.status(200).send({ id: idNota });
    }

    getNota(idNota) {
        return Nota.findById(idNota);
    }

}