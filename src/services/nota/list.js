'use strict';

const { Nota } = require('../../model/nota');
const { User } = require('../../model/user');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class NotaListService {

    // async process(req, res) {

    //     const userId = req.params.userId;

    //     if(!userId)
    //         return res.status(400).send({ mensaje: 'El id del usuario es requerido' });

    //     if(!isValidObjectId(userId))
    //         return res.status(400).send({ mensaje: 'El id del usuario debe ser de tipo ObjectId' });

    //     const user = await this.getUser(userId);

    //     if(!user)
    //         return res.status(400).send({ mensaje: `No existe un usuario con el id ${userId}` });

    //     const notas = await this.getNotas(userId);
    //     const notasFormatted = notas.map(nota => this.hideProperties(nota));

    //     return res.status(200).send(notasFormatted);
    // }

    async process(req, res) {

        const notas = await this.getNotas();
        const notasFormatted = notas.map(nota => this.hideProperties(nota));

        return res.status(200).send(notasFormatted);
    }


    async processActivas(req, res) {

        const notas = await this.getNotasActivas();
        const notasFormatted = notas.map(nota => this.hideProperties(nota));

        return res.status(200).send(notasFormatted);
    }


    async processActivasUsuario(req, res) {
        const idUsuarioBuscar = req.params.idUsuario;
        const notas = await this.getNotasActivasUsuario(idUsuarioBuscar);
        const notasFormatted = notas.map(nota => this.hideProperties(nota));

        return res.status(200).send(notasFormatted);
    }

    getNotasActivasUsuario(idUsuario) {
        const mysort = { prioridad: 1 };
        const estado = true;
         return Nota.find({ estado: estado , idUsuarioDestino: idUsuario}).sort(mysort);

    }
    
    getNotasActivas() {
        const  mysort = { prioridad: 1 };
        const estado = true;
         return Nota.find({ estado: estado }).sort(mysort);

    }
    
    getUser(userId) {
        return User.findById(userId);
    }

    //  getNotas(userId) {
    //     return Nota.find({ idUsuarioDestino: userId });
    // }

    getNotas(userId) {
        return Nota.find();
    }

    hideProperties(nota) {

        const { __v, _id, ...restOfNota } = nota._doc;
        restOfNota.id = _id;
    
        return restOfNota;
    }
}