'use strict';

const { Nota } = require('../../model/nota');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class NotaGetService {

    async process(req, res) {

        const idNota = req.params.id;

        if(!idNota)
            return res.status(400).send({ mensaje: 'El id del nota es requerido' });

        if(!isValidObjectId(idNota))
            return res.status(400).send({ mensaje: 'El id del nota debe ser de tipo ObjectId' });

        const nota = await this.getNota(idNota);

        if(!nota)
            return res.status(404).send({ mensaje: `Nota ${idNota} no encontrado` });

        const notaFormatted = this.hideProperties(nota);

        return res.status(200).send(notaFormatted);
    }

    getNota(idNota) {
        return Nota.findById(idNota);
    }

    hideProperties(nota) {

        const { __v, _id, ...restOfNota } = nota._doc;
        restOfNota.id = _id;
    
        return restOfNota;
    }
}