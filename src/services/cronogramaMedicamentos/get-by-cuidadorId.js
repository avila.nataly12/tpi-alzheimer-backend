'use strict';

const { CronogramaMedicamentos } = require('../../model/cronogramaMedicamentos');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class CronogramaMedicamentosGetByCuidadorIdService {

    async process(req, res) {

        const idCuidador = req.params.idCuidador;

        if(!idCuidador)
            return res.status(400).send({ mensaje: 'El id del cuidador es requerido' });

        if(!isValidObjectId(idCuidador))
            return res.status(400).send({ mensaje: 'El id del cuidador debe ser de tipo ObjectId' });

        const cronogramaMedicamentos = await this.getCronogramaMedicamentosByUser(idCuidador);

        if(!cronogramaMedicamentos)
            return res.status(404).send({ mensaje: `Cronograma de medicamentos no encontrado` });

        const cronogramaMedicamentosFormatted = cronogramaMedicamentos.map(cronogramaMedicamento => this.hideProperties(cronogramaMedicamento));

        return res.status(200).send(cronogramaMedicamentosFormatted);
    }

    getCronogramaMedicamentosByUser(idCuidador) {
        return CronogramaMedicamentos.find({ idUsuarioOrigen: idCuidador });
    }

    hideProperties(cronogramaMedicamentos) {

        const { __v, _id, ...restOfCronogramaMedicamentos } = cronogramaMedicamentos._doc; 
        restOfCronogramaMedicamentos.id = _id;
    
        return restOfCronogramaMedicamentos;
    }
}