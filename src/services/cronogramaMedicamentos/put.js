'use strict';

const { CronogramaMedicamentos } = require('../../model/cronogramaMedicamentos');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class CronogramaMedicamentosPutService {

    async process(req, res) {

        const idCronogramaMedicamentos = req.params.id;

        if(!idCronogramaMedicamentos)
            return res.status(400).send({ mensaje: 'El id del cronograma de medicamentos es requerido' });

        if(!isValidObjectId(idCronogramaMedicamentos))
            return res.status(400).send({ mensaje: 'El id del cronograma de medicamentos debe ser de tipo ObjectId' });

        const cronogramaMedicamentos = await this.getCronogramaMedicamentos(idCronogramaMedicamentos);

        if(!cronogramaMedicamentos)
            return res.status(404).send({ mensaje: `Cronograma de medicamentos con id ${idCronogramaMedicamentos} no encontrado` });

        const items = req.body.items;

        if(!items?.length)
            return res.status(400).send({ mensaje: 'Se debe enviar al menos un item' });

        cronogramaMedicamentos.items.push(...items);

        await cronogramaMedicamentos.save();

        return res.status(200).send({ id: idCronogramaMedicamentos });
    }

    getCronogramaMedicamentos(idCronogramaMedicamentos) {
        return CronogramaMedicamentos.findById(idCronogramaMedicamentos);
    }
}