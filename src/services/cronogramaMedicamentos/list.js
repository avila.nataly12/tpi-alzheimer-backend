'use strict';

const { CronogramaMedicamentos } = require('../../model/cronogramaMedicamentos');

module.exports = class CronogramaMedicamentosListService {

    async process(req, res) {

        const cronogramaMedicamentos = await this.getCronogramaMedicamentos();
        const cronogramaMedicamentosFormatted = cronogramaMedicamentos.map(cronogramaMedicamento => this.hideProperties(cronogramaMedicamento));

        return res.status(200).send(cronogramaMedicamentosFormatted);
    }

    getCronogramaMedicamentos() {
        return CronogramaMedicamentos.find();
    }

    hideProperties(cronogramaMedicamentos) {

        const { __v, _id, ...restOfCronogramaMedicamentos } = cronogramaMedicamentos._doc;
        restOfCronogramaMedicamentos.id = _id;
    
        return restOfCronogramaMedicamentos;
    }
}