'use strict';

const { CronogramaMedicamentos } = require('../../model/cronogramaMedicamentos');
const { User } = require('../../model/user');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class CronogramaMedicamentosPostService {

    async process(req, res) {

        const { isBadRequest, errorMesagge } = await this.validateRequest(req);

        if(isBadRequest)
            return res.status(400).send({ mensaje: errorMesagge });

        const cronogramaMedicamentosCreated = await this.createCronogramaMedicamentos(req);

        return res.status(200).send({ mensaje: 'Cronograma de medicamentos creado', id: cronogramaMedicamentosCreated._id });
    }

    async validateRequest(req) {

        let isBadRequest = false;
        let errorMesagge = '';

        const { idUsuarioOrigen, idUsuarioDestino, items } = req.body;

        if(!idUsuarioOrigen || !idUsuarioDestino) {

            isBadRequest = true;
            errorMesagge = 'El id del usuario de origen y el usuario de destino son requeridos';

            return { isBadRequest, errorMesagge };
        }

        if(!isValidObjectId(idUsuarioOrigen) || !isValidObjectId(idUsuarioDestino)) {

            isBadRequest = true;
            errorMesagge = 'El id del usuario de origen y el usuario de destino deben ser de tipo ObjectId';

            return { isBadRequest, errorMesagge };
        }

        const areValidUsersIds = await this.validateUserIds(idUsuarioOrigen, idUsuarioDestino);

        if(!areValidUsersIds) {
            isBadRequest = true;
            errorMesagge = 'Ids de usuarios (origen - destino) invalidos';

            return { isBadRequest, errorMesagge };
        }

        if(items && !Array.isArray(items)) {

            isBadRequest = true;
            errorMesagge = 'El array de items (fecha y medicamento) es obligatorio';

            return { isBadRequest, errorMesagge };
        }

        return { isBadRequest, errorMesagge };
    }

    async validateUserIds(idUsuarioOrigen, idUsuarioDestino) {

        const userIds = [...new Set([idUsuarioOrigen, idUsuarioDestino])];
        const userQuantity = userIds.length;

        if(userQuantity === 1) {
            const userFound = await User.findById(idUsuarioOrigen);

            if(userFound)
                return true;
        }

        if(userQuantity === 2) {
            const [userOrigenFound, userDestinoFound] = await Promise.all([
                User.findById(idUsuarioOrigen),
                User.findById(idUsuarioDestino),
            ]);

            if(userOrigenFound && userDestinoFound)
                return true;
        }

        return false;
    }

    createCronogramaMedicamentos(req) {

        const cronogramaMedicamentosToCreate = new CronogramaMedicamentos(req.body);

        return cronogramaMedicamentosToCreate.save();
    }
}