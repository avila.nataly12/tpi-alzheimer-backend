'use strict';

const { CronogramaMedicamentos } = require('../../model/cronogramaMedicamentos');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class CronogramaMedicamentosGetByPacienteIdService {

    async process(req, res) {

        const idPaciente = req.params.idPaciente;

        if(!idPaciente)
            return res.status(400).send({ mensaje: 'El id del paciente es requerido' });

        if(!isValidObjectId(idPaciente))
            return res.status(400).send({ mensaje: 'El id del paciente debe ser de tipo ObjectId' });

        const cronogramaMedicamentos = await this.getCronogramaMedicamentosByUser(idPaciente);

        if(!cronogramaMedicamentos)
            return res.status(404).send({ mensaje: `Cronograma de medicamentos no encontrado` });

        const cronogramaMedicamentosFormatted = this.hideProperties(cronogramaMedicamentos);

        return res.status(200).send([cronogramaMedicamentosFormatted]);
    }

    getCronogramaMedicamentosByUser(idPaciente) {
        return CronogramaMedicamentos.findOne({ idUsuarioDestino: idPaciente });
    }

    hideProperties(cronogramaMedicamentos) {

        const { __v, _id, ...restOfCronogramaMedicamentos } = cronogramaMedicamentos._doc;
        restOfCronogramaMedicamentos.id = _id;
    
        return restOfCronogramaMedicamentos;
    }
}