'use strict';

const { CronogramaMedicamentos } = require('../../model/cronogramaMedicamentos');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class CronogramaMedicamentosGetService {

    async process(req, res) {

        const idCronogramaMedicamentos = req.params.id;

        if(!idCronogramaMedicamentos)
            return res.status(400).send({ mensaje: 'El id del cronograma de medicamentos es requerido' });

        if(!isValidObjectId(idCronogramaMedicamentos))
            return res.status(400).send({ mensaje: 'El id del cronograma de medicamentos debe ser de tipo ObjectId' });

        const cronogramaMedicamentos = await this.getCronogramaMedicamentos(idCronogramaMedicamentos);

        if(!cronogramaMedicamentos)
            return res.status(404).send({ mensaje: `Cronograma de medicamentos ${idCronogramaMedicamentos} no encontrado` });

        const cronogramaMedicamentosFormatted = this.hideProperties(cronogramaMedicamentos);

        return res.status(200).send(cronogramaMedicamentosFormatted);
    }

    getCronogramaMedicamentos(idCronogramaMedicamentos) {
        return CronogramaMedicamentos.findById(idCronogramaMedicamentos);
    }

    hideProperties(cronogramaMedicamentos) {

        const { __v, _id, ...restOfCronogramaMedicamentos } = cronogramaMedicamentos._doc;
        restOfCronogramaMedicamentos.id = _id;
    
        return restOfCronogramaMedicamentos;
    }
}