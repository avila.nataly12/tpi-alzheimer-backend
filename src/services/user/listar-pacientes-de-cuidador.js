'use strict';

const { User } = require('../../model/user');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class UserListarPacientesDeCuidadorService {

    async process(req, res) {

        const idCuidador = req.params.idCuidador;

        if(!idCuidador)
            return res.status(400).send({ mensaje: 'El id del cuidador es requerido' });

        if(!isValidObjectId(idCuidador))
            return res.status(400).send({ mensaje: 'El id del cuidador debe ser de tipo ObjectId' });

        const cuidador = await this.getCuidador(idCuidador);

        if(!cuidador)
            return res.status(400).send({ mensaje: `No existe un cuidador con el id ${idCuidador}` });

        if(cuidador.rol !== 'Cuidador')
            return res.status(400).send({ mensaje: `El usuario ${idCuidador} no es un cuidador` });

        if(!cuidador.idsPacientes?.length)
            return res.status(400).send({ mensaje: `El cuidador ${idCuidador} aun no tiene pacientes asignados` });

        const pacientes = await this.getPacientes(cuidador.idsPacientes);
        const pacientesFormatted = pacientes.map(paciente => this.hideProperties(paciente));

        return res.status(200).send(pacientesFormatted);
    }

    getCuidador(idCuidador) {
        return User.findById(idCuidador);
    }

    getPacientes(idsPacientes) {
        return User.find({ _id: idsPacientes });
    }

    hideProperties(paciente) {

        const { __v, _id, ...restOfPaciente } = paciente._doc;
        restOfPaciente.id = _id;
    
        return restOfPaciente;
    }
}