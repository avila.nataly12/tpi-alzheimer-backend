'use strict';

const bcrypt = require('bcrypt');

const { User } = require('../../model/user');

const { transporter } = require('../../nodemailer/config');

const GenerarStringAleatorio = require('../../helpers/generar-string-aleatorio');

module.exports = class UserRecuperarContrasenaService {

    async process(req, res) {

        const email = req.body.email;

        if(!email)
            return res.status(400).send({ mensaje: 'El email es requerido' });

        const user = await this.getUser(email);

        if(!user)
            return res.status(400).send({ mensaje: `No existe un usuario con el mail ${email}` });

        const contrasenaNueva = GenerarStringAleatorio(10);
        const contrasenaEncriptada = await bcrypt.hash(contrasenaNueva, 12);

        user.contrasena = contrasenaEncriptada;
        await user.save();

        await transporter.sendMail(this.estructuraDelEmail(user, contrasenaNueva));

        return res.status(200).send({ mensaje: 'Nueva contraseña enviada al email' });
    }

    getUser(email) {
        return User.findOne({ email });
    }

    estructuraDelEmail(user, contrasenaNueva) {
        return {
            from: '"Alzheimer APP" <angelo11413@gmail.com>',
            to: user.email,
            subject: "Nueva contraseña - Alzheimer APP",
            html: `<h1>Generación de nueva contraseña</h1>
            <h2>Hola ${user.nombre}</h2>
            <p>Se ha generado una nueva contraseña para su usario, la cual es: ${contrasenaNueva}</p>`
        }
    }
}