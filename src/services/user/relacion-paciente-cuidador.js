'use strict';

const { User } = require('../../model/user');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class UserRelacionCuidadorPacienteService {

    async process(req, res) {

        const { isBadRequest, errorMesagge } = await this.validateRequest(req);

        if(isBadRequest)
            return res.status(400).send({ mensaje: errorMesagge });

        await Promise.all([this.updateCuidador(), this.updatePaciente()])

        return res.status(200).send({ mensaje: 'Cuidador y paciente actualizados correctamente' });
    }

    async validateRequest(req) {

        let isBadRequest = false;
        let errorMesagge = '';

        const {
            idCuidador, dniPaciente
        } = req.body;

        if(!idCuidador || !dniPaciente) {

            isBadRequest = true;
            errorMesagge = 'El id del cuidador y el DNI del paciente son requeridos';

            return { isBadRequest, errorMesagge };
        }

        if(!isValidObjectId(idCuidador)) {

            isBadRequest = true;
            errorMesagge = 'El id del cuidador debe ser de tipo ObjectId';

            return { isBadRequest, errorMesagge };
        }

        this.cuidador = await this.validateCuidador(idCuidador);

        if(!this.cuidador) {
            isBadRequest = true;
            errorMesagge = `Cuidador con id ${idCuidador} no encontrado`;

            return { isBadRequest, errorMesagge };
        }

        if(this.cuidador.rol !== 'Cuidador') {
            isBadRequest = true;
            errorMesagge = `El usuario con id ${idCuidador} no es un cuidador`;

            return { isBadRequest, errorMesagge };
        }

        this.paciente = await this.validatePaciente(dniPaciente);

        if(!this.paciente) {
            isBadRequest = true;
            errorMesagge = `Paciente con dni ${dniPaciente} no encontrado`;

            return { isBadRequest, errorMesagge };
        }

        if(this.paciente.rol !== 'Paciente') {
            isBadRequest = true;
            errorMesagge = `El usuario con dni ${dniPaciente} no es un paciente`;

            return { isBadRequest, errorMesagge };
        }

        return { isBadRequest, errorMesagge };
    }

    validateCuidador(idCuidador) {
        return User.findById(idCuidador);
    }

    validatePaciente(dniPaciente) {
        return User.findOne({ dni: dniPaciente })
    }

    updateCuidador() {
        this.cuidador.idsPacientes ? this.cuidador.idsPacientes.push(this.paciente._id) : this.cuidador.idsPacientes = [this.paciente._id];
        return this.cuidador.save()
    }

    updatePaciente() {
        this.paciente.idCuidador = this.cuidador._id;
        return this.paciente.save()
    }
}