'use strict';

const { UltimaUbicacion } = require('../../model/ultima-ubicacion');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class UltimaUbicacionPutService {

    async process(req, res) {

        const idPaciente = req.params.idPaciente;

        if(!idPaciente)
            return res.status(400).send({ mensaje: 'El id del paciente es requerido para encontrar la ubicacion actual' });

        if(!isValidObjectId(idPaciente))
            return res.status(400).send({ mensaje: 'El id del paciente debe ser de tipo ObjectId' });

        const ultimaUbicacion = await this.getUltimaUbicacion(idPaciente);

        if(!ultimaUbicacion)
            return res.status(404).send({ mensaje: `No se encontro la ultima ubicacion del paciente ${idPaciente}` });

        const nuevaUltimaUbicacion = req.body.ubicacion;

        if(!nuevaUltimaUbicacion)
            return res.status(400).send({ mensaje: "Indicar la nueva ultima ubicacion es requerido" });

        ultimaUbicacion.ubicacion = nuevaUltimaUbicacion;

        await ultimaUbicacion.save();

        return res.status(200).send({ id: ultimaUbicacion._id });
    }

    getUltimaUbicacion(idPaciente) {
        return UltimaUbicacion.findOne({ idPaciente });
    }
}