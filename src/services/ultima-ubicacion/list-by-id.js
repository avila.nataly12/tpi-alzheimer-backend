'use strict';

const { UltimaUbicacion } = require('../../model/ultima-ubicacion');
const { User } = require('../../model/user');

const isValidObjectId = require('../../helpers/validate-object-id');

module.exports = class UltimaUbicacionListService {

    async process(req, res) {

        const userId = req.params.id;
        if (!userId)
            return res.status(400).send({ mensaje: 'El id del usuario es requerido' });

        if (!isValidObjectId(userId))
            return res.status(400).send({ mensaje: 'El id del usuario debe ser de tipo ObjectId' });

        const user = await this.getUser(userId);

        if (!user)
            return res.status(400).send({ mensaje: `No existe un usuario con el id ${userId}` });

        const ultimaubicacion = await this.getUltimaUbicacion(userId);

        return res.status(200).send(ultimaubicacion);

    }

    getUser(userId) {
        return User.findById(userId);
    }

    getUltimaUbicacion(userId) {
        return UltimaUbicacion.findOne({ idPaciente: userId });
    }
}