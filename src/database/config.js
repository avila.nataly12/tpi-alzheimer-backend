const mongoose = require('mongoose');
const express = require('express');
const cors = require('cors');
const app = express();

const ALZHEIMER_MONGODB_CONEXION = 'mongodb+srv://angelo13:C4r4c4s.13.x@alzheimer-cluster.c1vr7es.mongodb.net/Alzheimer-Database';

const server = require('http').createServer(app);

const io = require('socket.io')(server);

const dbConexion = async () => {
    try {

        await mongoose.connect(ALZHEIMER_MONGODB_CONEXION, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });

        console.log('Conexión exitosa con la base de datos.');

    } catch (error) {

        console.log('Error en la conexion con la base de datos, ERROR: ', error);
        throw new Error('Error en la conexion con la base de datos, ERROR: ', error);
    }
}

// TODO: llevarlo a otra carpeta 
const socketConexion = async () => {
    try {

        await io.on('connection', () => {console.log('Conexion socket')});
    } catch (error) {
        
        console.log('Error en la conexion con el socket, ERROR: ', error);
        throw new Error('Error en la conexion con el socket, ERROR: ', error);
    }
}

module.exports = { dbConexion, socketConexion }