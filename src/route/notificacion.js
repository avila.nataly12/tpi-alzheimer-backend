'use strict';

const express = require('express');

const { notificacion } = require('../controller/notificacion');

const router = express.Router();

// Necesitan autenticacion 
router.get('/notificacion', notificacion); 
module.exports = router;