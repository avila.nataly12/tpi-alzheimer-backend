'use strict';

const express = require('express');

const { validateJwt } = require('../helpers/validate-jwt');
const { datosSaludPost, datosSaludGet, datosSaludListByPaciente,datosSaludListByPacienteFechas, datosSaludPut } = require('../controller/datosSalud');

const router = express.Router();

router.post('/', datosSaludPost);
router.get('/:id', datosSaludGet);
router.get('/paciente/:idPaciente', datosSaludListByPaciente);
router.get('/paciente/fechas/:idPaciente/:desde/:hasta', datosSaludListByPacienteFechas);
router.put('/:id', datosSaludPut);

module.exports = router;