const express = require('express');
const { validateJwt } = require('../helpers/validate-jwt');
const { 
    userList, userGet, userPost, userLogin, verifyUser,
    userRecuperarContrasena,sendSwpush,verificoSwpush,buscoPorDni,
    userRelacionCuidadorPaciente, userListarPacientesDeCuidador,saveSwpush
} = require('../controller/user');

const router = express.Router();

router.post('/', userPost);
router.post('/notificaciones/save/swpush', saveSwpush);
router.post('/notificaciones/send/swpush', sendSwpush);
router.get('/notificaciones/verifico/swpush/:idUsuario', verificoSwpush);
router.post('/login', userLogin);
router.get('/verificacion/:token', verifyUser);
router.post('/recuperar-contrasena', userRecuperarContrasena);

router.get('/', userList);
router.get('/:id', userGet);

router.post('/relacionar-cuidador-paciente', userRelacionCuidadorPaciente);
router.get('/listar-pacientes-de-cuidador/:idCuidador', userListarPacientesDeCuidador);
router.get('/dni/:dni', buscoPorDni);

module.exports = router;