'use strict';

const express = require('express');

const { ultimaubicacionList, ultimaUbicacionPut } = require('../controller/ultima-ubicacion');

const router = express.Router();

router.get('/:id', ultimaubicacionList);
router.put('/:idPaciente', ultimaUbicacionPut);

module.exports = router;