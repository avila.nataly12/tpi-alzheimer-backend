'use strict';

const express = require('express');

const { validateJwt } = require('../helpers/validate-jwt');
const { notaListUsuarios,notaListActivas , notaPost, notaGet, notaList, notaPut } = require('../controller/nota');

const router = express.Router();

// Necesitan autenticacion
router.post('/', notaPost);
router.get('/:id', notaGet);
router.get('/', notaList);
router.get('/estado/activo', notaListActivas);
router.get('/usuario/:idUsuario', notaListUsuarios);
router.put('/:id', notaPut);
//router.get('/listar/:userId', notaList);

module.exports = router;