'use strict';

const express = require('express');

const { validateJwt } = require('../helpers/validate-jwt');
const { enviarmensaje,enviar } = require('../controller/whatsapp');

const router = express.Router();

// Necesitan autenticacion 
router.get('/enviarmensaje', enviarmensaje); 
router.post('/enviar', enviar); 
module.exports = router;