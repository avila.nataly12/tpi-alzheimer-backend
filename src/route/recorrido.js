'use strict';

const express = require('express');

const { validateJwt } = require('../helpers/validate-jwt');
const { recorridoPost, recorridoGet, recorridoList, recorridoListAll, recorridoPut } = require('../controller/recorrido');

const router = express.Router();

// Necesitan autenticacion
router.post('/', recorridoPost);
router.get('/:id', recorridoGet);
router.get('/listar/:userId', recorridoList);
router.get('/', recorridoListAll);
router.put('/:id', recorridoPut);

module.exports = router;