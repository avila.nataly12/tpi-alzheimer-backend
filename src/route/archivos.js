'use strict';

const express = require('express');

const { validateJwt } = require('../helpers/validate-jwt');
const { dameUnArchivo,verFotoPerfil,verMultimedia,cambiarestado,archivoDelete,archivoPost,archivoListUsuarios ,archivoListUsuariosAlbum } = require('../controller/archivo');

const router = express.Router();

router.get('/verMultimedia/estado/true/:idUsuarioOrigen', verMultimedia); 
router.get('/:idUsuario/:idAlbum', archivoListUsuariosAlbum); 
router.get('/verFotoPerfil/porUsuario/:nombre', verFotoPerfil); 
router.get('/uno/:idUsuario/:idAlbum', dameUnArchivo); 


router.post('/', archivoPost); 
router.post('/cambiarestado/:id', cambiarestado); 
router.delete('/eliminar/:id', archivoDelete); 

module.exports = router;