'use strict';

const express = require('express');

const { validateJwt } = require('../helpers/validate-jwt');
const { listMemoria,quesoyList,quesoyListExcepto } = require('../controller/quesoy');

const router = express.Router();

router.get('/list', quesoyList); 
router.get('/list/:palabra', quesoyListExcepto); 
router.get('/list/nivel/:nivel', listMemoria); 

module.exports = router;