'use strict';

const express = require('express');

const { validateJwt } = require('../helpers/validate-jwt');
const { albumPost, albumGetById, albumListByCuidador, albumListByPaciente, albumPut } = require('../controller/album');

const router = express.Router();

router.post('/', albumPost);
router.get('/:id', albumGetById);
router.get('/cuidador/:idCuidador', albumListByCuidador);
router.get('/paciente/:idPaciente', albumListByPaciente);
router.put('/:id', albumPut);

module.exports = router;