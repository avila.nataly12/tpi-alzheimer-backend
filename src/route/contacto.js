'use strict';

const express = require('express');

const { validateJwt } = require('../helpers/validate-jwt');
const { getContacto,contactoPost,contactoIdOrigen, contactoUsuarios, contactoPut , boton,listadoOrigen,listadoOrigenCuidador,listadoOrigenPaciente } = require('../controller/contacto');

const router = express.Router();

router.post('/crear', contactoPost);
router.get('/getContacto/:idContacto', getContacto);
router.get('/origen/:idOrigen/:idPaciente', contactoIdOrigen);
router.get('/usuario/:idPaciente', contactoUsuarios);
router.put('/editar/:id', contactoPut);
router.get('/boton/:idPaciente', boton);
router.get('/origen/listado/boton/:idOrigen', listadoOrigen);
router.get('/origen/listado/boton/cuidador/:idOrigen', listadoOrigenCuidador);
router.get('/origen/listado/boton/paciente/:idOrigen', listadoOrigenPaciente);

module.exports = router;