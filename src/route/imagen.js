'use strict';

const express = require('express');

const { validateJwt } = require('../helpers/validate-jwt');
const { imagenPost, imagenGetById, imagenListByAlbumId, imagenPut } = require('../controller/imagen');

const router = express.Router();

router.post('/', imagenPost);
router.get('/:id', imagenGetById);
router.get('/album/:idAlbum', imagenListByAlbumId);
router.put('/:id', imagenPut);

module.exports = router;