'use strict';

const express = require('express');
const { validateJwt } = require('../helpers/validate-jwt');

const {
    cronogramaMedicamentosPost,
    cronogramaMedicamentosGet,
    cronogramaMedicamentosList,
    cronogramaMedicamentosPut,
    cronogramaMedicamentosGetByPacienteId,
    cronogramaMedicamentosGetByCuidadorId,
    cronogramaMedicamentosPutCambiarEstadoAFalse
} = require('../controller/cronogramaMedicamentos');

const router = express.Router();

router.post('/', cronogramaMedicamentosPost);
router.get('/:id', cronogramaMedicamentosGet);
router.get('/', cronogramaMedicamentosList);
router.put('/:id', cronogramaMedicamentosPut);
router.get('/paciente/:idPaciente', cronogramaMedicamentosGetByPacienteId);
router.get('/cuidador/:idCuidador', cronogramaMedicamentosGetByCuidadorId);
router.put('/:id/eliminarMedicamento', cronogramaMedicamentosPutCambiarEstadoAFalse);

module.exports = router;