'use strict';

const express = require('express');

const { validateJwt } = require('../helpers/validate-jwt');
const {dameReportePorUsuarioFecha,dameReportePorUsuario,dameReporte,juegoReporte,dameJuego,list,listHabilitados,juegoPost } = require('../controller/juegos');

const router = express.Router();

router.get('/dame/Juego/:idJuego', dameJuego); 
router.post('/', juegoPost); 
router.post('/reporte', juegoReporte); 

router.get('/list/reportePorFecha/:idJuego/:idPaciente/:desde/:hasta', dameReportePorUsuarioFecha); 
router.get('/list/reporte/:idJuego/:idPaciente', dameReportePorUsuario); 
router.get('/list/reporte/', dameReporte); 
router.get('/list', list); 
router.get('/habilitados/:idUsuario', listHabilitados); 
module.exports = router;