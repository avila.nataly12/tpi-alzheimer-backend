const mongoose = require('mongoose');

const DatosSalud = mongoose.model('DatosSalud', {
    idPaciente: {
        type: String,
        required: [true, 'El id del paciente es obligatorio']
    },
    idCuidador: {
        type: String,
        required: [true, 'El id del cuidador es obligatorio']
    },
    presionsistolica: {
        type: Number,
        required: [true, 'El indice de presion es obligatorio']
    },
    presiondiastolica: {
        type: Number,
        required: [true, 'El indice de presion es obligatorio']
    },
    oxigeno: {
        type: Number,
        required: [true, 'El indice de oxigeno es obligatorio']
    },
    temperatura: {
        type: Number,
        required: [true, 'El indice de la temperatura es obligatorio']
    },
    fecha: {
        type: String,
        required: [true, 'La fecha es obligatoria']
    },
    hora: {
        type: String,
        required: [true, 'La hora es obligatoria']
    },
    estado: {
        type: Boolean,
        default: true
    },
    fechaCreacion: {
        type: String,
        default: new Date().toISOString()
    }
});

module.exports = { DatosSalud };