const mongoose = require('mongoose');

const Reporte = mongoose.model('reportes', {
    idJuego: {
        type: String,
        default: ''
    },
    idPaciente: {
        type: String,
        default: ''
    },
    duracion: {
        type: String,
        default: ''
    },
    nivel: {
        type: String,
        default: ''
    },
    fecha: {
        type: String,
        default: ''
    } ,
    termino: {
        type: Boolean,
        default: false
    },
    fechaCreacion: {
        type: String,
        default: new Date().toISOString()
    }, 
});

module.exports = { Reporte };