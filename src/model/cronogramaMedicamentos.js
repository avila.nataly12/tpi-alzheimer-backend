const mongoose = require('mongoose');

const CronogramaMedicamentos = mongoose.model('CronogramaMedicamentos', {
    idUsuarioOrigen: {
        type: String,
        required: [true, 'El id del usuario de origen es obligatorio']
    },
    idUsuarioDestino: {
        type: String,
        required: [true, 'El id del usuario de destino es obligatorio']
    },
    items: {
        type: [],
        default: []
    },
    estado: {
        type: Boolean,
        default: true
    },
    fechaCreacion: {
        type: String,
        default: new Date().toISOString()
    }
});

module.exports = { CronogramaMedicamentos };