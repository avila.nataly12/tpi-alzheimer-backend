const mongoose = require('mongoose');

const Archivo = mongoose.model('Archivos', {
    nombre: {
        type: String,
        default: ''
    },
    idAlbum: {
        type: String,
        default: ''
    },
    idUsuario: {
        type: String,
        default: ''
    },
    ruta: {
        type: String,
        default: ''
    },
    estado: {
        type: Boolean,
        default: false
    },
    idfirebase: {
        type: String,
        default: ''
    },
    idUsuarioOrigen: {
        type: String,
        default: ''
    },
    formato: {
        type: String,
        default: ''
    },
    fechaCreacion: {
        type: String,
        default: new Date().toISOString()
    }
});

module.exports = { Archivo };