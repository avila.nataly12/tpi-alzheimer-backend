'use strict';

const express = require('express');
const cors = require('cors');

const { dbConexion, socketConexion } = require('../database/config');

const userRoutes = require('../route/user');
const recorridoRoutes = require('../route/recorrido');
const whatsappRoutes = require('../route/whatsapp');
const notificacionRoutes = require('../route/notificacion');
const notaRoutes = require('../route/nota');
const quesoyRoutes = require('../route/quesoy');
const juegosRoutes = require('../route/juegos');
const contactoRoutes = require('../route/contacto');
const archivosRoutes = require('../route/archivos');
const cronogramaMedicamentosRoutes = require('../route/cronogramaMedicamentos')
const datosSaludRoutes = require('../route/datosSalud');
const ultimaubicacionRoutes = require('../route/ultima-ubicacion');
const albumRoutes = require('../route/album');
const imagenRoutes = require('../route/imagen');

module.exports = class Server {
 
    constructor() {
        this.app = express();
        this.port = 8080;
        this.connectDb();
        this.activateMiddlewares();
        this.activateRoutes();
        this.connectSocket();
    }

    connectDb() {
        return dbConexion();
    }

    activateMiddlewares() {
        this.app.use(cors({ origin: true }));
        this.app.use(express.json());
    }

    activateRoutes() {
        this.app.use('/usuario', userRoutes);
        this.app.use('/recorrido', recorridoRoutes);
        this.app.use('/whatsapp', whatsappRoutes);
        this.app.use('/notificacion', notificacionRoutes);
        this.app.use('/nota', notaRoutes);
        this.app.use('/quesoy', quesoyRoutes);
        this.app.use('/juegos', juegosRoutes);
        this.app.use('/archivo', archivosRoutes);
        this.app.use('/contacto', contactoRoutes);
        this.app.use('/cronograma-medicamentos', cronogramaMedicamentosRoutes);
        this.app.use('/datos-salud', datosSaludRoutes);
        this.app.use('/ultima-ubicacion', ultimaubicacionRoutes);
        this.app.use('/album', albumRoutes);
        this.app.use('/imagen', imagenRoutes);
    }

    listen() {
        this.app.listen(this.port, () => console.log(`Servidor online en el puerto ${this.port}.`));
    }

    connectSocket() {
        return socketConexion();
    }
}