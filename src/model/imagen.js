const mongoose = require('mongoose');

const Imagen = mongoose.model('Imagens', {
    url: {
        type: String,
        required: [true, 'La url de la imagen es requerida']
    },
    descripcion: {
        type: String,
        required: [true, 'La descripcion de la imagen es requerida']
    },
    idAlbum: {
        type: String,
        required: [true, 'El id del album al que pertenece la imagen es obligatorio']
    },
    estado: {
        type: Boolean,
        default: true
    },
    fechaCreacion: {
        type: String,
        default: new Date().toISOString()
    }
});

module.exports = { Imagen };