const mongoose = require('mongoose');

const Nota = mongoose.model('Notas', {
    idUsuarioOrigen: {
        type: String,
        required: [true, 'El id del usuario de origen es obligatorio']
    },
    idUsuarioDestino: {
        type: String,
        required: [true, 'El id del usuario de destino es obligatorio']
    },
    contenido: {
        type: String,
        required: [true, 'El contenido de la nota es obligatorio']
    },
    fechaCreacion: {
        type: String,
        default: new Date().toISOString()
    },
    prioridad: {
        type: Number,
        Enumerator: [0, 1, 2, 3, 4, 5],
        default: 0
    },
    estado: {
        type: Boolean,
        default: true
    },
    urlImagen: {
        type: String,
        default: ''
    },
    titulo: {
        type: String,
        required: [true, 'El titulo de la nota es obligatorio']
    }
});

module.exports = { Nota };