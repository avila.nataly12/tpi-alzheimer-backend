const mongoose = require('mongoose');

const Juego = mongoose.model('juegos', {
    nombre: {
        type: String,
        default: ''
    },
    descripcion: {
        type: String,
        default: ''
    },
    foto: {
        type: String,
        default: ''
    },
    estado: {
        type: Boolean,
        default: false
    } 
});

module.exports = { Juego };