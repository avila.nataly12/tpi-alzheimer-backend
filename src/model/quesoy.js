const mongoose = require('mongoose');

const Quesoy = mongoose.model('quesoys', {
  
    nombre: {
        type: String,
        default: ''
    },
    estado: {
        type: Boolean,
        default: false
    } 
});

module.exports = { Quesoy };