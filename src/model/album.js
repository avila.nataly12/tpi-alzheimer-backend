const mongoose = require('mongoose');

const Album = mongoose.model('Albums', {
    nombre: {
        type: String,
        required: [true, 'El nombre del album es requerido']
    },
    idUsuarioOrigen: {
        type: String,
        required: [true, 'El id del usuario de origen es obligatorio']
    },
    idUsuarioDestino: {
        type: String,
        required: [true, 'El id del usuario de destino es obligatorio']
    },
    estado: {
        type: Boolean,
        default: true
    },
    fechaCreacion: {
        type: String,
        default: new Date().toISOString()
    }
});

module.exports = { Album };