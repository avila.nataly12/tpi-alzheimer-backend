const mongoose = require('mongoose');

const JuegoHabilitados = mongoose.model('juegoshabilitados', {
  
    idUsuario: {
        type: String,
        default: ''
    },
    idJuego: {
        type: String,
        default: ''
    },
    nivel: {
        type: String,
        default: ''
    } 
});

module.exports = { JuegoHabilitados };