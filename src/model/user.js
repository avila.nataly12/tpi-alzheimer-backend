const mongoose = require('mongoose');

const User = mongoose.model('Users', {
    nombre: {
        type: String,
        required: [true, 'El nombre del usuario es obligatorio']
    },
    apellido: {
        type: String,
        required: [true, 'El apellido del usuario es obligatorio']
    },
    dni: {
        type: String,
        required: [true, 'El DNI del usuario es obligatorio']
    },
    telefono: {
        type: String,
        required: [true, 'El número telefónico del usuario es obligatorio']
    },
    direccion: {
        type: String,
        required: [true, 'La dirección del usuario es obligatoria']
    },
    coordsdir: {
        type: []
    },
    rol: {
        type: String,
        Enumerator: ['Paciente', 'Cuidador', 'Admin', 'Institución'],
        required: [true, 'El ROL del usuario es obligatorio']
    },
    email: {
        type: String,
        required: [true, 'El email del usuario es obligatorio']
    },
    contrasena: {
        type: String,
        required: [true, 'La contraseña del usuario es obligatoria']
    },
    estado: {
        type: String,
        Enumerator: ['Pendiente', 'Activo'],
        default: 'Pendiente'
    },
    codigo: { 
        type: String
    },
    idsPacientes: {
        type: [],
        required: [false,'']
    },
    idCuidador: {
        type: String,
        required: [false, '']
    }
});

module.exports = { User };