const mongoose = require('mongoose');

const Contacto = mongoose.model('Contactos', {
    idPaciente: {
        type: String
    }, 
    nombre: {
        type: String
        },
    telefono: {
        type: String
        }, 
    estado: {
        type: Boolean,
        default: true
    },
    paciente: {
        type: Boolean,
        default: false
        }, 
    cuidador: {
    type: Boolean,
    default: false
    }, 
    idOrigen: {
    type: String
     }, 
});

module.exports = { Contacto };