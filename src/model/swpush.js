const mongoose = require('mongoose');

const Swpush = mongoose.model('Swpushs', {
    idUsuario: {
        type: String,
        required: [true, 'El id del usuario de origen es obligatorio']
    },
    clave: {
        type: Object,
        required: [true, 'clave es obligatorio']
    },
    fechaCreacion: {
        type: String,
        default: new Date().toISOString()
    }
});

 
module.exports = { Swpush };