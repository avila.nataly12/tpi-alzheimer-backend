const mongoose = require('mongoose');

const UltimaUbicacion = mongoose.model('UltimaUbicacion', {
    idPaciente: {
        type: String,
        required: [true, 'El id del paciente es obligatorio']
    },
    ubicacion: {
        type: [],
        required: [true, 'La ubicacion es obligatoria']
    }
});

module.exports = { UltimaUbicacion };