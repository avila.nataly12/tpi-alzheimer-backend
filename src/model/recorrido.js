const mongoose = require('mongoose');

const Recorrido = mongoose.model('Recorridos', {
    idUsuarioOrigen: {
        type: String,
        required: [true, 'El id del usuario de origen es obligatorio']
    },
    idUsuarioDestino: {
        type: String,
        required: [true, 'El id del usuario de destino es obligatorio']
    },
    estado: {
        type: String,
        required: [true, 'El estado del recorrido es obligatorio']
    },
    coordenadas: {
        type: [],
        required: [true, 'Las coordenadas del recorrido son obligatorias']
    },
    fechaCreacion: {
        type: String,
        default: new Date().toISOString()
    },
    fechaEstimadaPartida: {
        type: String,
        required: [true, 'La fecha estimada de partida es obligatoria']
    },
    fechaEstimadaLlegada: {
        type: String,
        required: [true, 'La fecha estimanada de llegada es obligatoria']
    },
    horaEstimadaPartida: {
        type: String,
        required: [true, 'La hora estimada de partida es obligatoria']
    },
    horaEstimadaLlegada: { 
        type: String,
        required: [true, 'La hora estimada de llegada es obligatoria']
    }
});

module.exports = { Recorrido };