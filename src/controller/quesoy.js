'use strict';

const QueSoyListService = require('../services/quesoy/list');
 
/* Controlador para listar notas filtrados por el id del usuario asignado */
const quesoyList = (req, res) => {
    try {

        const queSoyListService = new QueSoyListService();
        return queSoyListService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const quesoyListExcepto = (req, res) => {
    try {

        const queSoyListService = new QueSoyListService();
        return queSoyListService.processExcepto(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo123', error });
    }
};


const listMemoria = (req, res) => {
    try {

        const queSoyListService = new QueSoyListService();
        return queSoyListService.processListMemoria(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};


module.exports = { listMemoria,quesoyListExcepto ,  quesoyList }