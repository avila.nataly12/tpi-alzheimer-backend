'use strict';

const RecorridoPostService = require('../services/recorrido/post');
const RecorridoGetService = require('../services/recorrido/get');
const RecorridoListService = require('../services/recorrido/list');
const RecorridoListAllService = require('../services/recorrido/list-all');
const RecorridoPutService = require('../services/recorrido/put');

/* Controlador para crear un recorrido */
const recorridoPost = (req, res) => {
    try {

        const recorridoPostService = new RecorridoPostService();
        return recorridoPostService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

/* Controlador para obtener un recorrido por su id */
const recorridoGet = (req, res) => {
    try {

        const recorridoGetService = new RecorridoGetService();
        return recorridoGetService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

/* Controlador para listar recorridos filtrados por el id del usuario asignado */
const recorridoList = (req, res) => {
    try {

        const recorridoListService = new RecorridoListService();
        return recorridoListService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

/* Controlador para listar todos recorridos */
const recorridoListAll = (req, res) => {
    try {

        const recorridoListAllService = new RecorridoListAllService();
        return recorridoListAllService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const recorridoPut = (req, res) => {
    try {

        const recorridoPutService = new RecorridoPutService();
        return recorridoPutService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

module.exports = { recorridoPost, recorridoGet, recorridoList, recorridoListAll, recorridoPut }