'use strict';

const WhatsapPostService = require('../services/whatsapp/post'); 

const enviarmensaje = (req, res) => {
    try {

        const whatsapPostService = new WhatsapPostService();
        console.log(req);

        return whatsapPostService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const enviar = (req, res) => {
    try {

        const whatsapPostService = new WhatsapPostService();
        return whatsapPostService.processEnviar(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

module.exports = { enviarmensaje ,enviar }
