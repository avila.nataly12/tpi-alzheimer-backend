'use strict';

const ArchivoPostService = require('../services/archivo/post'); 
const ArchivoListService = require('../services/archivo/list'); 
const ArchivoDeleteService = require('../services/archivo/delete'); 


const archivoPost = (req, res) => {
    try {

        const archivoPostService = new ArchivoPostService();
        return archivoPostService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};



const cambiarestado = (req, res) => {
    try {

        const archivoPostService = new ArchivoPostService();
        return archivoPostService.processCambiarEstado(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const archivoDelete = (req, res) => {
    try {

        const archivoDeleteService = new ArchivoDeleteService();
        return archivoDeleteService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};


/* listar archivos por album*/
const archivoListUsuarios = (req, res) => {
    try {

        const archivoListService = new ArchivoListService();
        return archivoListService.processActivasUsuario(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};


const dameUnArchivo = (req, res) => {
    try {

        const archivoListService = new ArchivoListService();
        return archivoListService.buscounoarchivo(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};



const archivoListUsuariosAlbum = (req, res) => {
    try {

        const archivoListService = new ArchivoListService();
        return archivoListService.processActivasUsuarioAlbum(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};


const verFotoPerfil = (req, res) => {
    try {

        const archivoListService = new ArchivoListService();
        return archivoListService.processverFotoPerfil(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};


const verMultimedia = (req, res) => {
    try {

        const archivoListService = new ArchivoListService();
        return archivoListService.processverMultimedia(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};



module.exports = { dameUnArchivo,verFotoPerfil,verMultimedia,cambiarestado,archivoDelete,archivoPost,archivoListUsuarios,archivoListUsuariosAlbum }