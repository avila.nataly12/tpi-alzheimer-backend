'use strict';

const UltimaUbicacionListService = require('../services/ultima-ubicacion/list-by-id');
const UltimaUbicacionPutService = require('../services/ultima-ubicacion/put');

/* Controlador para obtener ultima ubicacion por id del paciente */
const ultimaubicacionList = (req, res) => {
    try {

        const ultimaubicacionListService = new UltimaUbicacionListService();
        return ultimaubicacionListService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const ultimaUbicacionPut = (req, res) => {
    try {

        const ultimaUbicacionPutService = new UltimaUbicacionPutService();
        return ultimaUbicacionPutService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

module.exports = { ultimaubicacionList, ultimaUbicacionPut }

