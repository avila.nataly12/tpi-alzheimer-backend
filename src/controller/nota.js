'use strict';

const NotaPostService = require('../services/nota/post');
const NotaGetService = require('../services/nota/get');
const NotaListService = require('../services/nota/list');
const NotaPutService = require('../services/nota/put');

/* Controlador para crear un nota */
const notaPost = (req, res) => {
    try {

        const notaPostService = new NotaPostService();
        return notaPostService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

/* Controlador para obtener un nota por su id */
const notaGet = (req, res) => {
    try {

        const notaGetService = new NotaGetService();
        return notaGetService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

/* Controlador para listar notas filtrados por el id del usuario asignado */
const notaList = (req, res) => {
    try {

        const notaListService = new NotaListService();
        return notaListService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const notaListActivas = (req, res) => {
    try {

        const notaListService = new NotaListService();
        return notaListService.processActivas(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};



const notaListUsuarios = (req, res) => {
    try {

        const notaListService = new NotaListService();
        return notaListService.processActivasUsuario(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

/* Controlador para actualizar una nota por id */
const notaPut = (req, res) => {
    try {

        const notaPutService = new NotaPutService();
        return notaPutService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

module.exports = { notaListUsuarios,notaListActivas, notaPost, notaGet, notaList, notaPut }