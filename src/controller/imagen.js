'use strict';

const ImagenPostService = require('../services/imagen/post'); 
const ImagenGetByIdService = require('../services/imagen/getById'); 
const ImagenListByAlbumIdService = require('../services/imagen/listByAlbumId'); 
const ImagenPutService = require('../services/imagen/put'); 

const imagenPost = (req, res) => {
    try {

        const imagenPostService = new ImagenPostService();
        return imagenPostService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const imagenGetById = (req, res) => {
    try {

        const imagenGetByIdService = new ImagenGetByIdService();
        return imagenGetByIdService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const imagenListByAlbumId = (req, res) => {
    try {

        const imagenListByAlbumIdService = new ImagenListByAlbumIdService();
        return imagenListByAlbumIdService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const imagenPut = (req, res) => {
    try {

        const imagenPutService = new ImagenPutService();
        return imagenPutService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

module.exports = { imagenPost, imagenGetById, imagenListByAlbumId, imagenPut }