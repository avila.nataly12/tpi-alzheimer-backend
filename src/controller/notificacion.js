'use strict';
const NotificacionPostService = require('../services/notificacion/post'); 

const webpush = require('web-push');
const express = require('express');
 const bodyParser = require('body-parser');

 const notificacion = (req, res) => {
    try {

        const notificacionPostService = new NotificacionPostService();
        return notificacionPostService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
  

};
 

module.exports = { notificacion  }