'use strict';

const ContactoPostService = require('../services/contacto/post');
const ContactoGetService = require('../services/contacto/get');
const ContactoPutService = require('../services/contacto/put');

 const contactoPost = (req, res) => {
    try {

        const contactoPostService = new ContactoPostService();
        return contactoPostService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

 const contactoUsuarios = (req, res) => {
    try {

        const contactoGetService = new ContactoGetService();
        return contactoGetService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};



const contactoIdOrigen = (req, res) => {
    try {

        const contactoGetService = new ContactoGetService();
        return contactoGetService.processIdOrigen(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};
const getContacto = (req, res) => {
    try {

        const contactoGetService = new ContactoGetService();
        return contactoGetService.dameContacto(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const listadoOrigen = (req, res) => {
    try {

        const contactoGetService = new ContactoGetService();
        return contactoGetService.processlistadoOrigen(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};


const listadoOrigenCuidador = (req, res) => {
    try {

        const contactoGetService = new ContactoGetService();
        return contactoGetService.processlistadoOrigenCuidador(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const listadoOrigenPaciente = (req, res) => {
    try {

        const contactoGetService = new ContactoGetService();
        return contactoGetService.processlistadoOrigenPaciente(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};


 const contactoPut = (req, res) => {
    try {

        const contactoPutService = new ContactoPutService();
        return contactoPutService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};



const boton = (req, res) => {
    try {

        const contactoGetService = new ContactoGetService();
        return contactoGetService.processBoton(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

module.exports = { getContacto,listadoOrigenPaciente,listadoOrigenCuidador,listadoOrigen,contactoPost,contactoUsuarios, contactoPut,contactoIdOrigen,boton }