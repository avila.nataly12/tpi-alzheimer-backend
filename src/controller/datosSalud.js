'use strict';

const DatosSaludPostService = require('../services/datosSalud/post');
const DatosSaludGetService = require('../services/datosSalud/get');
const DatosSaludListByPacienteService = require('../services/datosSalud/listByPaciente');
const DatosSaludPutService = require('../services/datosSalud/put');

/* Controlador para crear datos de salus */
const datosSaludPost = (req, res) => {
    try {

        const datosSaludPostService = new DatosSaludPostService();
        return datosSaludPostService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

/* Controlador para obtener datos de salud por su id */
const datosSaludGet = (req, res) => {
    try {

        const datosSaludGetService = new DatosSaludGetService();
        return datosSaludGetService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

/* Controlador para listar datos de salud filtrados por el id del paciente */
const datosSaludListByPaciente = (req, res) => {
    try {

        const datosSaludListByPacienteService = new DatosSaludListByPacienteService();
        return datosSaludListByPacienteService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const datosSaludListByPacienteFechas = (req, res) => {
    try {

        const datosSaludListByPacienteService = new DatosSaludListByPacienteService();
        return datosSaludListByPacienteService.processFechas(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const datosSaludPut = (req, res) => {
    try {

        const datosSaludPutService = new DatosSaludPutService();
        return datosSaludPutService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

module.exports = { datosSaludListByPacienteFechas,datosSaludPost, datosSaludGet, datosSaludListByPaciente, datosSaludPut }