'use strict';

const JuegosListService = require('../services/juegos/list');
const JuegosPostService = require('../services/juegos/post');
 

const juegoPost = (req, res) => {
    try {

        const juegoPostService = new JuegosPostService();
        return juegoPostService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};


const juegoReporte = (req, res) => {
    try {

        const juegoPostService = new JuegosPostService();
        return juegoPostService.processReporte(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};



 const list = (req, res) => {
    try {

        const juegosListService = new JuegosListService();
        return juegosListService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};


const listHabilitados = (req, res) => {
    try {

        const juegosListService = new JuegosListService();
        return juegosListService.processHabilitados(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const dameJuego = (req, res) => {
    try {

        const juegosListService = new JuegosListService();
        return juegosListService.processDameJuego(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};



const dameReporte = (req, res) => {
    try {

        const juegosListService = new JuegosListService();
        return juegosListService.reportes(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};



 


const dameReportePorUsuario = (req, res) => {
    try {

        const juegosListService = new JuegosListService();
        return juegosListService.reportesPorUsuario(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};



const dameReportePorUsuarioFecha = (req, res) => {
    try {

        const juegosListService = new JuegosListService();
        return juegosListService.reportesPorUsuarioFecha(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};



module.exports = {dameReportePorUsuarioFecha,dameReportePorUsuario, dameReporte,dameJuego,list,listHabilitados,juegoPost,juegoReporte }