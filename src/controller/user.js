const bcrypt = require('bcrypt');
const JWT = require('jsonwebtoken');

const { User } = require('../model/user');
const { UltimaUbicacion } = require('../model/ultima-ubicacion');
const { transporter } = require('../nodemailer/config');

const UserRelacionCuidadorPacienteService = require('../services/user/relacion-paciente-cuidador');
const UserListarPacientesDeCuidadorService = require('../services/user/listar-pacientes-de-cuidador');
const UserRecuperarContrasenaService = require('../services/user/recuperar-contrasena');
const SwpushService = require('../services/swpush/post');

/* Controlador para listar todos los usuarios */
const userList = async (req, res) =>{
    try {

        const usuarios = await User.find();

        return res.status(200).send(usuarios.map(ocultarPropiedadesUsuario));

    } catch (error) {

        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

/* Controlador para listar un usuario por su id */
const userGet = async (req, res) => {
    try {

        const { id } = req.params;

        if(!esObjectIdValido(id))
            return res.status(400).send({ mensaje: `El identificador ${id} es invalido` });
        
        const usuario = await User.findById(id);

        if(!usuario)
            return res.status(404).send({ mensaje: `No existe el usuario ${id}` });

        return res.status(200).send(ocultarPropiedadesUsuario(usuario));
        
    } catch (error) {

        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const buscoPorDni = async (req, res) => {
    try {

        const { dni } = req.params;

          
        const usuario = await User.findOne({dni:dni});

        if(!usuario)
            return res.status(404).send({ mensaje: `No existe el usuario con el dni ${dni}` });
     
          
        return res.status(200).send(usuario);
        
    } catch (error) {

        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};



/* Controlador para registrar un nuevo usuario */
const userPost = async (req, res) => {
    try {

        if(!poseeLasPropiedadesRequeridas(req.body))
            return res.status(400).send({ mensaje: 'Las propiedades [ nombre - apellido - dni - telefono - direccion - rol - email - contrasena ] son requeridas' });

        if(!esEmailValido(req.body.email))
            return res.status(400).send({ mensaje: `El email ${req.body.email} es invalido` });

        if(!esContrasenaValida(req.body.contrasena))
            return res.status(400).send({ mensaje: 'La contraseña debe contener mínimo ocho caracteres' });

        const usuarioExistente = await User.findOne({ email: req.body.email });

        if(usuarioExistente && usuarioExistente.estado === 'Activo')
            return res.status(400).send({ mensaje: `El email ${req.body.email} se encuentra registrado` });

        let usuario = new User(req.body);

        const contrasenaEncriptada = await bcrypt.hash(usuario.contrasena, 12);

        usuario.contrasena = contrasenaEncriptada;
        usuario.codigo = Math.round(Math.random()*999999);
        const usuarioCreado = await usuario.save();
        
        if(usuarioCreado.rol == 'Paciente') {
            let ultimaUbicacion = new UltimaUbicacion();
            ultimaUbicacion.idPaciente = usuarioCreado._id;
            ultimaUbicacion.ubicacion = [];

            await ultimaUbicacion.save();
        }

        const token = JWT.sign({ _id: usuarioCreado._id }, 'secretkey', { expiresIn: '168h' });

        await transporter.sendMail(estructuraDelEmail(usuarioCreado, token));

        return res.status(200).send({ mensaje: 'Usuario creado', id: usuarioCreado._id });

    } catch (error) {

        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

/* Controlador para loguear un usuario */
const userLogin = async (req, res) => {
    try {

        const { email, contrasena } = req.body;

        if(!email || !contrasena)
            return res.status(400).send({ mensaje: 'El email y la contrasena son requeridos para loguearse' })
    
        const usuario = await User.findOne({ email });
    
        if(!usuario || !bcrypt.compareSync(contrasena, usuario.contrasena))
            return res.status(400).send({ mensaje: 'Email o contraseña incorrecta' });
    
        if(usuario.estado !== 'Activo')
            return res.status(400).send({ mensaje: 'Debe verificar la cuenta para poder loguearse' });
    
        return res.status(200).send({
            token: JWT.sign({
                _id: usuario._id,
                nombre: `${usuario.nombre} ${usuario.apellido}`,
                rol: usuario.rol,
                dni: usuario.dni,
                telefono: usuario.telefono,
                email: usuario.email
            }, 'secretkey', { expiresIn: '24h' })
        });

    } catch (error) {

        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

/* Controlador para verificar la cuenta de un usuario */
const verifyUser = async (req, res) => {
    try {

        const { token } = req.params;

        if(!token)
            return res.status(400).send({ mensaje: 'Para verificar la cuenta debe de enviarse el token' });
    
        let payload;

        try {
            payload = JWT.verify(token, 'secretkey');
        } catch (error) {
            return res.status(400).send({ mensaje: 'Token invalido' })
        }

        const usuario = await User.findById(payload._id);

        if(!usuario)
            return res.status(400).send({ mensaje: 'Token invalido' });

        res.status(200).send({ mensaje: 'Verificacion realizada con exito' });
        usuario.estado = 'Activo';
        return usuario.save();

    } catch (error) {

        res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
}

const userRecuperarContrasena = (req, res) => {
    try {

        const userRecuperarContrasenaService = new UserRecuperarContrasenaService();
        return userRecuperarContrasenaService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const userRelacionCuidadorPaciente = (req, res) => {
    try {

        const userRelacionCuidadorPacienteService = new UserRelacionCuidadorPacienteService();
        return userRelacionCuidadorPacienteService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const userListarPacientesDeCuidador = (req, res) => {
    try {

        const userListarPacientesDeCuidadorService = new UserListarPacientesDeCuidadorService();
        return userListarPacientesDeCuidadorService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

/* Helpers */
const ocultarPropiedadesUsuario = usuario => {
    const { __v, contrasena, _id, ...restoDelUsuario } = usuario._doc;
    restoDelUsuario.id = _id;

    return restoDelUsuario;
}

const esObjectIdValido = id => {
    return id.match(/^[0-9a-fA-F]{24}$/);
}

const esEmailValido = email => {
    return email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
}

const esContrasenaValida = contrasena => {
    return contrasena.length >= 8;
}

const poseeLasPropiedadesRequeridas = propiedades => {

    const propiedadesRequeridas = ['nombre', 'apellido', 'dni', 'telefono', 'direccion', 'rol', 'email', 'contrasena'];
    let existenTodasLasPropiedadesRequeridas = true;

    propiedadesRequeridas.forEach(propiedad => {
        if(!propiedades[propiedad])
            existenTodasLasPropiedadesRequeridas = false;
    });

    return existenTodasLasPropiedadesRequeridas;
}

const estructuraDelEmail = (data, token) => {
    return {
        from: '"Alzheimer APP" <angelo11413@gmail.com>',
        to: data.email,
        subject: "Codigo de verificacion✔ - Alzheimer APP",
        html: `<h1>Confirmacion de correo electronico</h1>
        <h2>Hola ${data.nombre}</h2>
        <p>Para validar tu cuenta, ingresa en el siguiente enlace <a href="https://alfa-backend.rj.r.appspot.com/usuario/verificacion/${token}" target="_blank">confirmar cuenta</a></p>
        <p>El codigo tiene una validez de siete (7) dias, en caso de no confirmar antes del tiempo estipulado debe completar el formulario de registro nuevamente.</p>`
    }
}

const saveSwpush = (req, res) => {
    try {

        const swpushService = new SwpushService();
        return swpushService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const sendSwpush = (req, res) => {
    try {

        const swpushService = new SwpushService();
        return swpushService.processSend(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};



const verificoSwpush = (req, res) => {
    try {

        const swpushService = new SwpushService();
        return swpushService.processVerifico(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

module.exports = {
    userList, userGet, userPost, userLogin, verifyUser,buscoPorDni,
    userRecuperarContrasena,saveSwpush,sendSwpush,verificoSwpush,
    userRelacionCuidadorPaciente, userListarPacientesDeCuidador
}