'use strict';

const CronogramaMedicamentosPostService = require('../services/cronogramaMedicamentos/post');
const CronogramaMedicamentosGetService = require('../services/cronogramaMedicamentos/get');
const CronogramaMedicamentosListService = require('../services/cronogramaMedicamentos/list');
const CronogramaMedicamentosPutService = require('../services/cronogramaMedicamentos/put');
const CronogramaMedicamentosGetByPacienteIdService = require('../services/cronogramaMedicamentos/get-by-pacienteId');
const CronogramaMedicamentosGetByCuidadorIdService = require('../services/cronogramaMedicamentos/get-by-cuidadorId');
const CronogramaMedicamentosPutCambiarEstadoAFalseService = require('../services/cronogramaMedicamentos/put-cambiar-estado-a-false');

/* Controlador para crear un cronograma de medicamentos */
const cronogramaMedicamentosPost = (req, res) => {
    try {

        const cronogramaMedicamentosPostService = new CronogramaMedicamentosPostService();
        return cronogramaMedicamentosPostService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

/* Controlador para obtener un cronograma de medicamentos por su id */
const cronogramaMedicamentosGet = (req, res) => {
    try {

        const cronogramaMedicamentosGetService = new CronogramaMedicamentosGetService();
        return cronogramaMedicamentosGetService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

/* Controlador para listar cronograma de medicamentos */
const cronogramaMedicamentosList = (req, res) => {
    try {

        const cronogramaMedicamentosListService = new CronogramaMedicamentosListService();
        return cronogramaMedicamentosListService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

/* Controlador para actualizar un cronograma de medicamentos por id*/
const cronogramaMedicamentosPut = (req, res) => {
    try {

        const cronogramaMedicamentosPutService = new CronogramaMedicamentosPutService();
        return cronogramaMedicamentosPutService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const cronogramaMedicamentosGetByPacienteId = (req, res) => {
    try {

        const cronogramaMedicamentosGetByPacienteIdService = new CronogramaMedicamentosGetByPacienteIdService();
        return cronogramaMedicamentosGetByPacienteIdService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const cronogramaMedicamentosGetByCuidadorId = (req, res) => {
    try {

        const cronogramaMedicamentosGetByCuidadorIdService = new CronogramaMedicamentosGetByCuidadorIdService();
        return cronogramaMedicamentosGetByCuidadorIdService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const cronogramaMedicamentosPutCambiarEstadoAFalse = (req, res) => {
    try {

        const cronogramaMedicamentosPutCambiarEstadoAFalseService = new CronogramaMedicamentosPutCambiarEstadoAFalseService();
        return cronogramaMedicamentosPutCambiarEstadoAFalseService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};


module.exports = {
    cronogramaMedicamentosPost,
    cronogramaMedicamentosGet,
    cronogramaMedicamentosList,
    cronogramaMedicamentosPut,
    cronogramaMedicamentosGetByPacienteId,
    cronogramaMedicamentosGetByCuidadorId,
    cronogramaMedicamentosPutCambiarEstadoAFalse
}