'use strict';

const AlbumPostService = require('../services/album/post'); 
const AlbumGetByIdService = require('../services/album/getById'); 
const AlbumListByCuidadorService = require('../services/album/listByCuidador'); 
const AlbumListByPacienteService = require('../services/album/listByPaciente'); 
const AlbumPutService = require('../services/album/put'); 

const albumPost = (req, res) => {
    try {

        const albumPostService = new AlbumPostService();
        return albumPostService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const albumGetById = (req, res) => {
    try {

        const albumGetByIdService = new AlbumGetByIdService();
        return albumGetByIdService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const albumListByCuidador = (req, res) => {
    try {

        const albumListByCuidadorService = new AlbumListByCuidadorService();
        return albumListByCuidadorService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const albumListByPaciente = (req, res) => {
    try {

        const albumListByPacienteService = new AlbumListByPacienteService();
        return albumListByPacienteService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

const albumPut = (req, res) => {
    try {

        const albumPutService = new AlbumPutService();
        return albumPutService.process(req, res);

    } catch (error) {
        return res.status(500).send({ mensaje: 'Error interno, intente de nuevo', error });
    }
};

module.exports = { albumPost, albumGetById, albumListByCuidador, albumListByPaciente, albumPut }