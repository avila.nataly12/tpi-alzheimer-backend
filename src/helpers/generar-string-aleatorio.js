'use strict';

const banco = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

module.exports = longitudCadena => {

    let aleatoria = '';

    for (let i = 0; i < longitudCadena; i++) {
        aleatoria += banco.charAt(Math.floor(Math.random() * banco.length));
    }

    return aleatoria;
};