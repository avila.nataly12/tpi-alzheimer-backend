const JWT = require('jsonwebtoken');
const { User } = require('../model/user');

const MESSAGE = 'Solicitud denegada - Unauthorized';

const validateJwt = async (req, res, next) => {

    try {

        if(!req.headers.authorization || !req.headers.authorization.split(' ')[1])
            return res.status(401).send({ MESSAGE });

        const token = req.headers.authorization.split(' ')[1];

        let payload;

        try {
            payload = JWT.verify(token, 'secretkey');
        } catch (error) {
            return res.status(400).send({ MESSAGE });
        }

        const userFound = await User.findById(payload._id);

        if(!userFound)
            return res.status(401).send({ MESSAGE });

        req.userId = payload._id;
        next();
        
    } catch (error) {
        return res.status(500).send({ MESSAGE: 'Error', error });
    }
}

module.exports = { validateJwt }